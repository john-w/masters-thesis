%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- PETROV FUNDAMENTALS -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
    As seen in Section \ref{sec:diffGeometry}, the Riemman-Christoffel tensor 
    $R_{\mu\nu\alpha\beta}$ has the following symmetry relations

    \begin{subequations}
    \begin{align}
        \begin{split}
            R_{\mu\nu\alpha\beta} = - R_{\mu\nu\beta\alpha}
                                                                \label{pet:Riemann34antisym}
        \end{split}
        \,, \\
        \begin{split}
             R_{\mu\nu\alpha\beta} = - R_{\nu\mu\alpha\beta}
                                                                \label{pet:Riemann12antisym}
        \end{split}
        \,, \\
        \begin{split}
            R_{\mu\nu\alpha\beta} = R_{\alpha\beta\mu\nu} 
                                                                 \label{pet:Riemann12-34sym}
        \end{split}
        \,, \\
        \begin{split}
            R_{\mu\nu\alpha\beta} + R_{\mu\alpha\beta\nu} + R_{\mu\beta\nu\alpha} = 0
                                                             \label{pet:Riemann1stBianchiId}
        \end{split}
        \,,
    \end{align} 
                                                               \label{pet:RiemannSymmetries}
    \end{subequations}

    \noindent
    with $\mu = 0, 1, 2, 4$ in a 4-spacetime.  Those symmetries suggest an 
    underlying structure within \emph{pairs of indices}, so we can think the curvature 
    tensor as 

    \begin{align*}
        R_{(\mu\nu)(\alpha\beta)}
    \end{align*}

    \noindent
    where each pair $(\mu\nu)$ can be treated as individual entities altogether, 
    anti-symmetric between its constituent indices evidenced \eqref{pet:Riemann34antisym} 
    and \eqref{pet:Riemann12antisym}. In this way we can introduce a new index that spans 
    all the possible combinations of $\mu\nu$ that are anti-symmetric

    \begin{align}
        A \equiv (\mu\nu) = \{(01), (02), (03), (12), (23), (31)\}
                                                                 \label{pet:bivectorIndices}
                                                                 \,,
    \end{align}

    \noindent
    effectively giving rise to an equivalent 6 dimensional vector space composed of
    \emph{bi-vectors}%
    \footnote{Bi-vectors are essentially the most natural way to codify planes. For more
              information on the underlying theory, refer to Appendix \ref{apx:bivectors}.
             }%
    , which will be labelled by uppercase latin indices spanning $A = 0, \ldots, 5$.
    Raising and lowering pairs of indices is done with the rank 4 metric tensor

    \begin{align}
        g_{AB} \equiv
        g_{\mu\nu\sigma\rho} := g_{\mu\sigma} g_{\nu\rho} - g_{\mu\rho} g_{\nu\sigma}
                                                                  \label{pet:bivectorMetric}
    \end{align}

    \simbolo{$g_{AB}$}{Bi-vector metric tensor}

    \noindent
    which carries a signature $(- - - + + +)$ due to the original signature and to 
    \eqref{pet:bivectorIndices}. We also shall break the 6-index family into two, each one
    carrying each signed sector of the metric

    \begin{align}
        A = \begin{cases}
            I &:= \{0, 1, 2\} = \{(01),(02),(03)\} \\
            X &:= \{3, 4, 5\} = \{(12),(31),(23)\}
        \end{cases}
                                                           \label{pet:bivectorIndexFamilies}
                                                           \,,
    \end{align}

    \noindent
    where the former will be represented by letters starting from $I$ ($I, J, K, \cdots$)
    and the latter starting from $X$ ($X, Y, Z, \cdots$). This is completely analogous to
    how we split the time components ``${}^0$'' from spatial ones ``${}^i$'' in regular
    4-spacetimes, which will become evident when we start working with those bi-vector 
    objects.

    \

    From here on we will consider a locally inertial frame of reference, so

    \begin{align*}
        g_{\mu\nu} \rightarrow \eta_{\mu\nu}
        \,,
    \end{align*}

    \noindent
    which implies

    \begin{align*}
        g_{\mu\nu\alpha\beta} \rightarrow \eta_{\mu\nu\alpha\beta} \equiv \eta_{AB}
        \..
    \end{align*}

    Our interest lies on the \emph{vacuum solutions} such that

    \begin{align*}
        R_{\mu\nu} = 0
        \,,
    \end{align*}

    \noindent
    but this strongly restricts the class of solutions to a very particular case. We instead
    will work with the \emph{Weyl tensor} \eqref{con:weylTensor}, which is constructed in 
    such a way that $C_{\mu\nu} \equiv 0$ for any geometry. One nice consequence of using
    it is that all the results obtained in this chapter will also be valid for the
    \emph{conformal group of transformations}, expanding the validity of the theory. We
    leave the details of this symmetry group and its construction to Appendix
    \ref{apx:conformal}, for now it is enough to just use the Weyl tensor given by

    \simbolo{$C_{\mu\nu\alpha\beta}$}{Weyl tensor}

    \begin{align}
        \begin{split}
        C_{\mu\nu\alpha\beta}
        =
        R_{\mu\nu\alpha\beta}
        &+ \frac{1}{2}
        \bigg(
              R_{\mu\alpha} g_{\nu\beta}
            + R_{\mu\beta} g_{\nu\alpha}
            - R_{\nu\alpha} g_{\mu\beta}
            - R_{\nu\beta} g_{\mu\alpha}
        \bigg)
        \\
        &- \frac{1}{6}
        \bigg(
            g_{\mu\alpha} g_{\nu\beta} - g_{\mu\beta} g_{\nu\alpha}
        \bigg) R
        \end{split}
                                                                      \label{pet:weylTensor}
                                                                      \..
    \end{align}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{{{
\iffalse 
    It is
    worth then to construct a new tensor based upon $R_{\mu\nu\alpha\beta}$, its first
    and second contractions $R_{\mu\nu}$ and $R$, and the metric tensor $g_{\mu\nu}$,
    carrying the same indicial symmetries as the curvature tensor. So the most general
    tensor that can be constructed in said fashion is

    \begin{align*}
        C_{\mu\nu\alpha\beta} =
        R_{\mu\nu\alpha\beta} + A_{\mu\alpha} g_{\nu\beta} + A_{\nu\beta} g_{\mu\alpha}
                              - A_{\mu\beta} g_{\nu\alpha} - A_{\nu\alpha} g_{\mu\beta}
    \end{align*}

    \noindent
    where $A_{\mu\nu}$ is a symmetric rank 2 tensor to be determined. Contracting
    $\mu\beta$ and \emph{imposing} $C_{\nu\alpha} = 0$

    \begin{align*}
        C^\mu_{\nu\alpha\mu} \equiv C_{\nu\alpha} = 0
\iftoggle{steps}{
        &= 
        R_{\nu\alpha} + \tA[^\mu_\alpha] g_{\nu\mu} + \tA[_\nu_\mu] \delta^\mu_\alpha
                      - \tA[^\mu_\mu] g_{\nu\alpha} - \tA[_\nu_\alpha] \delta^\mu_\mu
                                                                                \nonumber \\
        &=
        R_{\nu\alpha} + \tA[_\nu_\alpha] + \tA[_\nu_\alpha]
                      - A g_{\nu\alpha} - 4\tA[_\nu_\alpha]
                                                                                \nonumber \\
        A_{\nu\alpha}
        &=
}{\implies}
        \frac{1}{2} \bigg(
            R_{\nu\alpha} - g_{\nu\alpha} A
        \bigg)
        \..
    \end{align*}

    Further contracting the remaining indices

    \begin{align*}
\iftoggle{steps}{
        A &= \frac{1}{2} \bigg( R - 4 A \bigg)
                                                                                \nonumber \\
        3A &= \frac{1}{2} R
                                                                                \nonumber \\
}
        A &= \frac{1}{6} R
        \,,
    \end{align*}

    \noindent
    and plugging it back on the expression above

    \begin{align*}
        A_{\nu\alpha} = \frac{1}{2} R_{\nu\alpha} - \frac{1}{12}g_{\nu\alpha} R
    \end{align*}

    \noindent
    so

    \begin{align}
        C_{\mu\nu\alpha\beta}
        &=
\iftoggle{steps}{
        R_{\mu\nu\alpha\beta}
        + \frac{1}{2}
        \bigg(
            R_{\mu\alpha} - \frac{1}{6} g_{\mu\alpha} R
        \bigg) g_{\nu\beta}
        + \frac{1}{2}
        \bigg(
            R_{\nu\beta} - \frac{1}{6} g_{\nu\beta} R
        \bigg) g_{\mu\alpha}
                                                                                \nonumber \\
        &\qquad\qquad\qquad
        - \frac{1}{2}
        \bigg(
            R_{\mu\beta} - \frac{1}{6} g_{\mu\beta} R
        \bigg) g_{\nu\alpha}
        - \frac{1}{2}
        \bigg(
            R_{\nu\alpha} - \frac{1}{6} g_{\nu\alpha} R
        \bigg) g_{\mu\beta}
                                                                                \nonumber \\
        &=
}\
        R_{\mu\nu\alpha\beta}
        + \frac{1}{2}
        \bigg(
              R_{\mu\alpha} g_{\nu\beta}
            + R_{\mu\beta} g_{\nu\alpha}
            - R_{\nu\alpha} g_{\mu\beta}
            - R_{\nu\beta} g_{\mu\alpha}
        \bigg)
        - \frac{1}{6}
        \bigg(
            g_{\mu\alpha} g_{\nu\beta} - g_{\mu\beta} g_{\nu\alpha}
        \bigg) R
                                                                      \label{pet:weylTensor}
                                                                      \,,
    \end{align}

    \noindent
    which is the so-called \emph{Weyl tensor} and it is also invariant under conformal
    transformations.
\fi
}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






    \

    Instead of working in a 6-dimensional space where the system of equations will have the
    same dimension, we can complexify it, reducing the dimension by half. That is done by 
    breaking the Weyl tensor into three bits; each with a fixed number of time components. 
    Naturally, those three quantities will be labeled by spatial indices. Thus, we define

    \begin{align}
        \begin{split}
        X_{ij} &:= C_{0i0j} 
        \\
        Y_{ij} &:= \frac{1}{2} \epsilon_{ilm} C_{0jlm}
        \\
        Z_{ij} &:= \frac{1}{4} \epsilon_{ilm} \epsilon_{jrs} C_{lmrs}
        \end{split}
                                                                        \label{pet:weylBits}
                                                                        \,,
    \end{align}

    \simbolo{$\epsilon_{ilm}$}{Totally anti-symmetric symbol}

    \noindent
    where both $X$ and $Z$ are symmetric and $Y$ can be anything. 
    
    \
 
    The vacuum condition $C_{\mu\nu} = 0$ is then codified as

\iftoggle{steps}{
    \begin{align*}
        C^\lambda_{\nu\alpha\lambda} &= 0
                                                                                \nonumber \\
        \implies
        C_{0\nu\alpha0} - C_{i\nu\alpha i} 
        &= 0
    \end{align*}

    \begin{align*}
        \cdot\
        C_{00} = 0 
        &= C_{0000} - C_{i00i}
        &&\rightarrow C_{0i0i} = 0 
        &&\implies X_{ii} = 0
                                                                                \nonumber \\
        \cdot\
        C_{0j} = 0
        &= C_{00j0} - C_{i0ji}
        &&\rightarrow C_{0iji} = 0 = \epsilon_{kji} Y_{ki} 
        &&\implies Y_{ij} = Y_{ji}
                                                                                \nonumber \\
    \end{align*}

    \begin{align*}
        \cdot\
        C_{jk} = 0
        &= C_{0jk0} - C_{ijki}
        \quad \rightarrow \quad
        C_{ijik} = C_{0j0k}
                                                                                \nonumber \\
                                                                                \nonumber \\
        C_{ijik} &= \epsilon_{lij} \epsilon_{mik} Z_{lm}
                                                                                \nonumber \\
        &= (Z_{ii} \delta_{jk} - Z_{jk}) = C_{0j0k} \equiv X_{jk}
        \operate{}{$contract$ jk}
                                                                                \nonumber \\
                                                                                \nonumber \\
        &Z_{ii} (3 - 1) = X_{ii} \equiv 0
                                                                                \nonumber \\
        &\therefore Z_{ii} = 0
                                                                                \nonumber \\
                                                                                \nonumber \\
        &\implies Z_{jk} = - X_{jk}
        \..
    \end{align*}

    Summarizing
}

    \begin{align}
        X_{ii} = 0
        \qquad \,; \qquad
        Y_{ij} = Y_{ji}
        \qquad \,; \qquad
        Z_{ij} = - X_{ij}
                                                                  \label{pet:weylBitsVacuum}
                                                                  \,,
    \end{align}

    \noindent
    so all of them are symmetric. Not only that, but they also have nil trace. Indeed, $X$
    and $Z$ are immediate, while 

    \begin{align*}
        Y_{ii} = C_{0123} + C_{0231} + C_{0312} \equiv 0
    \end{align*}

    \noindent
    due to the first Bianchi identity \eqref{pet:Riemann1stBianchiId}.

    \

    Componentwise, we can rewrite \eqref{pet:weylBits} considering the symmetries of the
    vacuum solution \eqref{pet:weylBitsVacuum} as

    \begin{subequations}
    \begin{align}
    \begin{split}
        [X_{ij}] &= \left(
        \begin{matrix}
            C_{0101} & C_{0102} & C_{0103} \\
            C_{0102} & C_{0202} & C_{0203} \\
            C_{0103} & C_{0203} & C_{0303}
        \end{matrix}
        \right)
    \end{split}
                                                                                          \\
    \begin{split}
        [Y_{ij}] &= \left(
        \begin{matrix}
            C_{0123} & C_{0131} & C_{0112} \\
            C_{0223} & C_{0231} & C_{0212} \\
            C_{0323} & C_{0331} & C_{0312} 
        \end{matrix}
        \right)
    \end{split}
                                                                                          \\
    \begin{split}
        [Z_{ij}] &= \left(
        \begin{matrix}
            C_{2323} & C_{2331} & C_{2312} \\
            C_{2331} & C_{3131} & C_{3112} \\
            C_{2312} & C_{3112} & C_{1212}
        \end{matrix}
        \right)
        = - [X_{ij}]
    \end{split}
                                                                                         \,,
    \end{align}
                                                                \label{pet:weylBitsMatrices}
    \end{subequations}

    \noindent
    which in the bi-vector space takes the form

    \begin{subequations}
    \begin{align}
    \begin{split}
        [X_{ij}] &= \left(
        \begin{matrix}
            C_{00} & C_{01} & C_{02} \\
            C_{01} & C_{11} & C_{12} \\
            C_{02} & C_{12} & C_{22}
        \end{matrix}
        \right) 
        \iff [C_{IJ}]
    \end{split}
    \,, \\
    \begin{split}
        [Y_{ij}] &= \left(
        \begin{matrix}
            C_{05} & C_{04} & C_{03} \\
            C_{15} & C_{14} & C_{13} \\
            C_{25} & C_{24} & C_{23}
        \end{matrix}
        \right)
        \iff [C_{IX}]
    \end{split}
    \,, \\
    \begin{split}
        [Z_{ij}] &= \left(
        \begin{matrix}
            C_{55} & C_{45} & C_{35} \\
            C_{45} & C_{44} & C_{43} \\
            C_{35} & C_{34} & C_{33}
        \end{matrix}
        \right)
        \iff [C_{XY}]
    \end{split}
    \..
    \end{align}
                                                       \label{pet:weylBitsMatricesBivectors}
    \end{subequations}

    Finally, the complexification is done by defining the complex tensor

    \begin{align*}
        W_{ij} = \frac{1}{2} \bigg( X_{ij} - Z_{ij} + 2i Y_{ij} \bigg)
    \end{align*}

    \simbolo{$W_{ij}$}{Complexified Weyl tensor}

    \noindent
    such that, in the vacuum,

    \begin{align}
        W_{ij} = X_{ij} + i Y_{ij}
                                                               \label{pet:complexWeylVacuum}
                                                               \,,
    \end{align}

    \noindent
    which is also nil traced, i.e. $W_{ii} = 0$.

    \

    We also shall be needing the invariants associated with the curvature tensor, but
    before doing anything, let us first digress a bit on the electromagnetic case. There
    are only two unique invariants in this case, which are given by

    \begin{align*}
        I_1 &= F_{\mu\nu} F^{\mu\nu} = E^2 - B^2
        \,, \\
        I_2 &= F_{\mu\nu} \ast F^{\mu\nu} = 2 \bs{E} \cdot \bs{B}
    \end{align*}

    \noindent 
    or in the bi-vector form

    \begin{align*}
        I_1 = F_A F^A
        \qquad \,; \qquad
        I_2 = F_A \ast F^A
        \,,
    \end{align*}

    \noindent
    where

    \begin{align*}
        F^{\mu\nu} = \left(
        \begin{matrix}
            0   &  E^1 &  E^2 &  E^3 \\
           -E^1 &  0   &  B^3 & -B^2 \\
           -E^2 & -B^3 &  0   &  B^1 \\
           -E^3 &  B^2 & -B^1 &  0   \\
        \end{matrix}
        \right)
    \end{align*}

    \noindent
    is the Maxwell tensor and 
    
    \begin{align*}
        \ast F^{\mu\nu} = \frac{1}{2} \epsilon^{\mu\nu\sigma\rho} F_{\sigma\rho}
         \rightarrow
        \ast F^A = \frac{1}{2} \epsilon^{AB} F_B
    \end{align*}

    \simbolo{$\ast F^A$}{Hodge dual}

    \noindent
    is the Hodge dual Maxwell tensor, with the bi-vector Levi-Civita symbol assuming the 
    only possible values%
    \footnote{It is interesting to note that in the bi-vector space the Levi-Civita is
              symmetric.}

    \begin{align*}
        \epsilon^{AB} =
        \bigg\{
            \epsilon^{05} = \epsilon^{50} = 1,\ 
            \epsilon^{14} = \epsilon^{41} = 1,\  
            \epsilon^{23} = \epsilon^{32} = 1
        \bigg\}
        \..
    \end{align*}

    Now, if we construct the complex vector

    \begin{align*}
        \bs{F} = \bs{E} + i \bs{B}
    \end{align*}

    \noindent
    and take its square

    \begin{align*}
        \bs{F}^2 = \bs{F} \cdot \bs{F} 
        &= (E^2 - B^2) + 2i \bs{E} \cdot \bs{B}
                                                                                \nonumber \\
        &= I_1 + i I_2
        \,,
    \end{align*}

    \noindent
    so the complex vector carries both invariants within it. Returning to the point in
    question, we expect the same behaviour to manifest with the complex tensor
    \eqref{pet:complexWeylVacuum}. The curvature tensor has four invariants:

    \begin{align}
        I_1 &= R_{AB} R^{AB}
        \qquad &&\,; \qquad
        I_2 = R_{AB} \ast R^{AB}
                                                                                \nonumber \\
        I_3 &= \tR[_A^B] \tR[_B^C] \tR[_C^A]
        \qquad &&\,; \qquad
        I_4 = \tR[_A^B] \tR[_B^C] \ast \tR[_C^A]
                                                               \label{pet:riemannInvariants}
                                                               \,,
    \end{align}

    \noindent
    with

    \begin{align*}
        \ast R_{AB} = \frac{1}{2} \epsilon_{AC} \tR[^C_B]
        \,,
    \end{align*}

    \noindent
    and so do the Weyl tensor, since it has the same symmetries. The complex invariants is
    then given by%
    \footnote{This is a construction, so the choice of $\pm i$ is arbitrary, but defining
    these invariants in this form simplifies the results later on.}

    \begin{align}
        \mathcal{I}_1 &= I_1 - i I_2
                                                                                \nonumber \\
        \mathcal{I}_2 &= I_3 + i I_4
                                                        \label{pet:riemannComplexInvariants}
                                                        \,,
    \end{align}

    \noindent
    which using \eqref{pet:weylBitsMatricesBivectors} gives%
%
\iftoggle{steps}{

    \begin{align*}
        I_1 &= C_{AB}C^{AB} = C_{IB}C^{IB} + C_{XB}C^{XB}
                                                                                \nonumber \\
        &=
        C_{IJ} C^{IJ} + 2 C_{IX} C^{IX} + C_{XY}C^{XY}
        \operate{}{$lower with $ \eta_{AB}}
                                                                                \nonumber \\
        &=
        C_{IJ} C_{IJ} - 2 C_{IX} C_{IX} + C_{XY}C_{XY}
                                                                                \nonumber \\
        &=
        X_{ij} X_{ij} + Z_{ij}Z_{ij} - 2 Y_{ij} Y_{ij}
                                                                                \nonumber \\
        &=
        2 \Tr\big( \mathds{X}^2 - \mathds{Y}^2 \big)
    \end{align*}

    \begin{align*}
        I_2 &= C_{AB} \ast C^{AB} = \frac{1}{2} C_{AB} \epsilon^{AC} \tC[_C^B] 2%
        \footnote{Redundant summed $\epsilon^{AB}$.}
                                                                                \nonumber \\
        &=
        C_{IB} \epsilon^{IX} \tC[_X^B] + C_{XB} \epsilon^{XI} \tC[_I^B]
                                                                                \nonumber \\
        &=
        2 C_{IB} \epsilon^{IX} \tC[_X^B]
                                                                                \nonumber \\
        &=
        2 \epsilon^{IX} ( C_{IJ} \tC[_X^J] + C_{IY} \tC[_X^Y] )
        \operate{}{$lower with $ \eta_{AB}}
                                                                                \nonumber \\
        &=
        2 \epsilon^{IX} ( C_{IJ} C_{XJ} - C_{IY} C_{XY} )
                                                                                \nonumber \\
        &=
        2 \bigg\{
            \big[
                C_{0Y} C_{5Y} + C_{1Y} C_{4Y} + C_{2Y} C_{3Y}
            \big]
            -
            \big[
                C_{0J} C_{5J} + C_{1J} C_{4J} + C_{2J} C_{3J}
            \big]
        \bigg\}
                                                                                \nonumber \\
        &=
        2 \bigg\{
            \big(
                C_{03} C_{53} + C_{04} C_{54} + C_{05} C_{55}
            \big)
            +
            \big(
                C_{13} C_{43} + C_{14} C_{44} + C_{15} C_{45}
            \big)
                                                                                \nonumber \\
            &\qquad\qquad
            +
            \big(
                C_{23} C_{33} + C_{24} C_{34} + C_{25} C_{35}
            \big)
            -
            \big(
                C_{00} C_{50} + C_{01} C_{51} + C_{02} C_{52}
            \big)
                                                                                \nonumber \\
            &\qquad\qquad
            -
            \big(
                C_{10} C_{40} + C_{11} C_{41} + C_{12} C_{42}
            \big)
            -
            \big(
                C_{20} C_{30} + C_{21} C_{31} + C_{22} C_{32}
            \big)
        \bigg\}
                                                                                \nonumber \\
        &=
        2 \bigg\{
              Y_{31} Z_{31} + Y_{21} Z_{21} + Y_{11} Z_{11} 
            + Y_{32} Z_{32} + Y_{22} Z_{22} + Y_{12} Z_{12}
                                                                                \nonumber \\
            &\qquad\qquad
            + Y_{33} Z_{33} + Y_{23} Z_{23} + Y_{13} Z_{13}
            - \big(
                  X_{11} Y_{11} + X_{21} Y_{12} + X_{31} Y_{13}
                                                                                \nonumber \\
            &\qquad\qquad
                + X_{12} Y_{21} + X_{22} Y_{22} + X_{32} Y_{23}
                + X_{13} Y_{31} + X_{23} Y_{32} + X_{33} Y_{33}
            \big)
        \bigg\}
                                                                                \nonumber \\
        &=
        2 \big( Y_{ij} Z_{ji} - X_{ij} Y_{ji} \big)
                                                                                \nonumber \\
        &=
        - 4 \Tr \big( \mathds{Y} \mathds{X} \big)
    \end{align*}

    \begin{align*}
        \therefore
        \mathcal{I}_1 = I_1 - i I_2 
        = 2 \Tr\bigg( \mathds{X}^2 - \mathds{Y}^2 \bigg) 
        + 4i\Tr\bigg( \mathds{Y}\mathds{X} \bigg)
    \end{align*}

    This is indeed analogous to the electromagnetic case, up to a scale factor,
    for if $\mathds{W} = \mathds{X} + i \mathds{Y}$

    \begin{align*}
        \Tr(\mathds{W}^2) 
        = 
        \Tr\big[(\mathds{X} + i \mathds{Y})(\mathds{X} + i \mathds{Y})\big]
        =
        \Tr(\mathds{X}^2 - \mathds{Y}^2) + 2i \Tr(\mathds{X}\mathds{Y})
        \..
    \end{align*}

    Now for the last two

    \begin{align*}
        I_3 &= \tC[_A^B] \tC[_B^C] \tC[_C^A]
        = \tC[_I^B] \tC[_B^C] \tC[_C^I] + \tC[_X^B] \tC[_B^C] \tC[_C^X]
                                                                                \nonumber \\
        &=
        ( \tC[_I^J] \tC[_J^C] \tC[_C^I] + \tC[_I^X] \tC[_X^C] \tC[_C^I] )
        +
        ( \tC[_X^I] \tC[_I^C] \tC[_C^X] + \tC[_X^Y] \tC[_Y^C] \tC[_C^X] )
                                                                                \nonumber \\
        &=
        ( \tC[_I^J] \tC[_J^K] \tC[_K^I] + \tC[_I^J] \tC[_J^X] \tC[_X^I] )
        +
        ( \tC[_I^X] \tC[_X^J] \tC[_J^I] + \tC[_I^X] \tC[_X^Y] \tC[_Y^I] )
                                                                                \nonumber \\
            &\qquad\qquad
        + 
        ( \tC[_X^I] \tC[_I^J] \tC[_J^X] + \tC[_X^I] \tC[_I^Y] \tC[_Y^X] )
        +
        ( \tC[_X^Y] \tC[_Y^I] \tC[_I^X] + \tC[_X^Y] \tC[_Y^Z] \tC[_Z^X] )
                                                                                \nonumber \\
        &=
            \tC[_I^J] \tC[_J^K] \tC[_K^I]
        + 3 \tC[_I^J] \tC[_J^X] \tC[_X^I]
        + 3 \tC[_X^Y] \tC[_Y^I] \tC[_I^X]
        +   \tC[_X^Y] \tC[_Y^Z] \tC[_Z^X]
                                                                                \nonumber \\
        &=
        -   C_{IJ} C_{JK} C_{KI}
        + 3 C_{IJ} C_{JX} C_{XI}
        - 3 C_{XY} C_{YI} C_{IX}
        +   C_{XY} C_{YZ} C_{ZX}
                                                                                \nonumber \\
        &=
        -   X_{ij} X_{jk} X_{ji}
        + 3 X_{ij} Y_{jk} Y_{ki}
        - 3 Z_{ij} Y_{jk} Y_{ki}
        +   Z_{ij} Z_{jk} Z_{ki}
                                                                                \nonumber \\
        &=
        - 2 \Tr\big( \mathds{X}^3 - 3 \mathds{X} \mathds{Y}^2 \big)
    \end{align*}

    \begin{align*}
        I_4 &= \tC[_A^B] \tC[_B^C] \ast \tC[_C^A]
        = \tC[_A^B] \tC[_B_C] \ast \tC[^C^A]
                                                                                \nonumber \\
        &=
        \tC[_A^B] \tC[_B_C] \epsilon^{CD} \tC[_D^A]
                                                                                \nonumber \\
        &=
        \bigg(
            \tC[_A^B] \tC[_B_I] \epsilon^{IX} \tC[_X^A]
            +
            \tC[_A^B] \tC[_B_X] \epsilon^{XI} \tC[_I^A]
        \bigg)
                                                                                \nonumber \\
        &= \epsilon^{IX}
        \bigg(
            \tC[_A^B] \tC[_B_I] \tC[_X^A]
            +
            \tC[_B^A] \tC[_A_X] \tC[_I^B]
        \bigg)
                                                                                \nonumber \\
        &= 2 \epsilon^{IX} \tC[_A^B] \tC[_B_I] \tC[_X^A]
                                                                                \nonumber \\
        &= 2 \epsilon^{IX} 
        \bigg(
              \tC[_A^J] \tC[_J_I] \tC[_X^A]
            + \tC[_A^Y] \tC[_Y_I] \tC[_X^A]
        \bigg)
                                                                                \nonumber \\
        &= 2 \epsilon^{IX} 
        \bigg[
            ( \tC[_K^J] \tC[_J_I] \tC[_X^K]
            + \tC[_Y^J] \tC[_J_I] \tC[_X^Y] )
            + 
            ( \tC[_J^Y] \tC[_Y_I] \tC[_X^J]
            + \tC[_Z^Y] \tC[_Y_I] \tC[_X^Z] )
        \bigg]
                                                                                \nonumber \\
        &= 2 \epsilon^{IX} 
        \bigg(
              C_{KJ} C_{JI} C_{XK}
            - C_{YJ} C_{JI} C_{XY} 
            - C_{JY} C_{YI} C_{XJ}
            + C_{ZY} C_{YI} C_{XZ} 
        \bigg)
                                                                                \nonumber \\
        &= 2 \Tr
        \bigg(
              \mathds{X} \mathds{X} \mathds{Y}
            - \mathds{Y} \mathds{X} \mathds{Z}
            - \mathds{Y} \mathds{Y} \mathds{Y}
            + \mathds{Z} \mathds{Y} \mathds{Z}
        \bigg)
                                                                                \nonumber \\
        &= 
        2 \Tr \bigg( 3 \mathds{X}^2 \mathds{Y} - \mathds{Y}^3 \bigg)
    \end{align*}

    \begin{align*}
        \therefore
        \mathcal{I}_2 = I_3 + i I_4
        =
        - 2 \Tr\bigg( \mathds{X}^3 - 3 \mathds{X} \mathds{Y}^2 \bigg)
        + 2i \Tr \bigg( 3 \mathds{X}^2 \mathds{Y} - \mathds{Y}^3 \bigg)
    \end{align*}
    
    Synthetizing%
}{}% 
\footnote{If $z = a + ib$ and $z' = -a + ib$, we see that $\mathcal{I}_1$ and
  $\mathcal{I}_2$ correspond to $z^2$ and $z'^3$, respectively. We would expect this  due to 
  the complex structure of this representation.}

    \begin{align}
        \mathcal{I}_1 
        &= 2 \Tr(\mathds{X}^2 - \mathds{Y}^2) + 4i\Tr(\mathds{Y}\mathds{X})
                                                                                \nonumber \\
        \mathcal{I}_2
        &= - 2 \Tr(\mathds{X}^3 - 3\mathds{X}\mathds{Y}^2)
           + 2i\Tr(3\mathds{X}^2 \mathds{Y} - \mathds{Y}^3)
                                                        \label{pet:riemannInvariantsComplex}
                                                        \..
    \end{align}

    Back to the main focus. Since the complex tensor \eqref{pet:complexWeylVacuum} fully
    describes the curvature in vacuum, we can analize its algebraic structure in terms of 
    the characteristic system associated with it, 

    \begin{align}
        \mathds{W} \bs{n}^{(k)} = \lambda^{(k)} \bs{n}^{(k)}
                                                          \label{pet:characteristicEquation}
                                                          \,,
    \end{align}

    \noindent
    for some complex 3-eigenvector $n^{(k)}_i$ associated with complex eigenvalues 
    $\lambda$, corresponding to the invariants of $\mathds{W}$ (and, by extension, of 
    $C_{\mu\nu\alpha\beta}$). Petrov\cite{ART:Petrov2000GReGr..32.1665P} calls 
    the eigenvectors the \emph{stationary directions} associated with the \emph{stationary
    curvatures}.

    \

    It will be useful to separate the real and imaginary parts from the eigenvalues

    \begin{align*}
        \lambda^{(k)} = \lambda'^{(k)} + i \lambda''^{(k)}
        \..
    \end{align*}

    Since $\mathds{W}$ has nil trace, the eigenvalues are constrained among themselves,
    that is,

    \begin{align}
        \lambda^{(1)} + 
        \lambda^{(2)} + 
        \lambda^{(3)} = 0
                                                       \label{pet:eigenvaluesConstraintment}
                                                       \..
    \end{align}

    There are only three possible classes of solutions of the  characteristic equation 
    \eqref{pet:characteristicEquation}, depending on the number of independent
    eigenvectors. Those classes define the types I-III of the \emph{Petrov
    classification}. There are also the subclasses $D$, $N$ and $O$ which are degenerated
    cases (one or more identical eigenvalues) of the former classes.

    \fancyend
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ ------------------- // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
