%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- BKL MODEL -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    Encontraremos as equações de Einstein no espaço homogêneo de tipo IX, caracterizado
    por

    \begin{align}
        n_i &\equiv \tn[^i^i] = 1 \ \ ; \ \ \tn[^i^j] = 0 \ \ ,\ i \neq j     \nonumber \\
        a &= 1                                                           \label{bkl:biaIX}
                                                                                       \,,
    \end{align}

    que gera as constantes de estrutura

    \begin{align}
        \tC[^i^i] &= 1 \ \ ; \ \ \tC[^i^j] = 0\ \ ,\ i \neq j                 \nonumber \\
        \tC[^1_2_3] &= \tC[^2_3_1] = \tC[^3_1_2] = 1               \label{bka:biaIXconsts}
                                                                                       \,,
    \end{align}

    que podem ser denotados simbolicamente por

    \begin{align}
        \tC[^i^j] = \tensor{\tilde{\eta}}{^i^j} \ \ ;\ \ 
        \tC[^a_b_c] = \tensor{\tilde{\epsilon}}{^a_b_c}                \label{bka:symbols}
                                                                                       \,,
    \end{align}

    onde essas quantidades não são tensores mas podemos operá-las como (abuso de
    linguagem).

    As equações de Einstein no referencial síncrono e expressa em termos das tetradas
    tomam a seguinte forma

   \begin{subequations}
   \begin{align}
       \begin{split}
           \frac{1}{2} \tensor{\dot{\chi}}{_{(a)}^{(a)}}
           + \frac{1}{4} \tchi[_{(a)}^{(b)}] \tchi[_{(b)}^{(a)}]
           &= - 8 \pi G \bigg( \tT[_0^0] - \frac{1}{2} T \bigg)
       \end{split}                                             \label{bkl:IXeinstein00} \\
%       
       \begin{split}
           0 
           &= -8 \pi G \tT[_{(a)}^0]
       \end{split}                                             \label{bkl:IXeinsteina0} \\
%       
       \begin{split}
           \tP{_{(a)}^{(b)}}
         - \frac{1}{2\sqrt{\gamma}} \tpartial[_t] 
                                  \bigg(\sqrt{\gamma}\tchi[_{(a)}^{(b)}] \bigg)
          &= - 8 \pi G \bigg(\tT[_{(a)}^{(b)}] - \frac{1}{2} \delta_a^b T \bigg)
       \end{split}                                                \label{bkl:IXeinsteinab}
                                                                                       \,,
   \end{align}
                                                           \label{bkl:IXeinsteinEquations}
   \end{subequations}

   onde $\gamma$ e $\tP[_{(a)}^{(b)}]$ representam a métrica e o tensor de Ricci
   puramente espaciais, respectivamente, e são definidas as quantidades

   \begin{subequations}
   \begin{align}
   \begin{split}
       \tchi[_a_b] &\equiv \tensor{\dot{\gamma}}{_a_b}
   \end{split}                                                       \label{bkl:chiDEF} \\
%   
   \begin{split}
       \tchi[_a^a] &= \tgamma[^c^b] \tensor{\dot{\gamma}}{_c_b}
   \end{split}                                                     \label{bkl:chiTrace}
   \end{align}
                                                                        \label{bkl:chi}
   \end{subequations}

   Na semana passada vimos que para as constantes de Bianchi IX, o tensor de Ricci é dado
   por
   \begin{align}
       \tP[_{(a)}^{(b)}] = \frac{1}{2\eta} \delta_a^b
                                                                        \label{bkl:Pab}
   \end{align}

   Na construção da teoria, vimos que devido a graus de liberdade extras que aparecem na
   determinação das constantes de estrutura, podemos sempre escolher uma métrica diagonal
   sem perda de generalidade. Aliás, esta é precisamente a primeira etapa do método de
   Belinskii, Khalatnikov e Lifshitz (BKL). 

   \

   $\rightarrow$ Ansatz:

   \begin{align}
       \tgamma[_a_b] &= a^2(t) \ \tetU{1}{a} \tetU{1}{b}
                      + b^2(t) \ \tetU{2}{a} \tetU{2}{b}
                      + c^2(t) \ \tetU{3}{a} \tetU{3}{b}
                                                                  \label{bkl:ansatzMetric}
   \end{align}

   Em primeira aproximação, consideraremos o cenário de espaço vazio, pois o tensor
   energia-momento é nulo e isso simplificará as contas, além de ser também interessante
   de estudar o comportamento desse tipo de espaço no vácuo. Logo

   \begin{subequations}
   \begin{align}
       \begin{split}
           \frac{1}{2} \tensor{\dot{\chi}}{_{(a)}^{(a)}}
           + \frac{1}{4} \tchi[_{(a)}^{(b)}] \tchi[_{(b)}^{(a)}]
           &= 0
       \end{split}                                               \label{bkl:IXvaccum00} \\
%       
       \begin{split}
           0 &= 0
       \end{split}                                               \label{bkl:IXvaccuma0} \\
%       
       \begin{split}
           \tP{_{(a)}^{(b)}}
           + \frac{1}{2} \tensor{\dot{\chi}}{_{(a)}^{(b)}}
           + \frac{1}{4} \bigg(
                 \tchi[_{(a)}^{(b)}] \tchi[_{(c)}^{(c)}]
             - 2 \tchi[_{(a)}^{(c)}] \tchi[_{(c)}^{(b)}]
           \bigg)
           &= 0
       \end{split}                                               \label{bkl:IXvaccumab}
                                                                                       \,.
   \end{align}
                                                                   \label{bkl:IXvaccum}
   \end{subequations}

   Imediatamente, \eqref{bkl:IXvaccuma0} é trivialmente satisfeito. Note que aqui estou
   utilizando a forma não simplificada de \eqref{bkl:IXvaccumab}, como mostrei num
   relatório anterior. Essa forma facilita os cálculos que faremos em breve.

   Note ainda que tanto a métrica \eqref{bkl:ansatzMetric} quanto o tensor de Ricci
   $\tP[_{(a)}^{(b)}]$ são diagonais, então apenas os termos diagonais de
   \eqref{bkl:IXvaccumab} são não nulos.

   \

   Vejamos agora termo a termo como ficam os termos remanescentes de
   \eqref{bkl:IXvaccum}.

   %  * * *  %
   \fancybreak
   %  * * *  %

   · Termo ${ }_0^0$

   \begin{align}
       \tchi[_a^a] &= \tgamma[^a^a] \tensor{\dot{\gamma}}{_a_a}               \nonumber \\
%       
       &= a^{-2} \tpartial_t(a^2) + b^{-2} \tpartial_t(b^2) + c^{-2} \tpartial_t(c^2)
                                                                              \nonumber \\
%       
       &= 2 \bigg(
           \frac{\dot{a}}{a} + \frac{\dot{b}}{b} + \frac{\dot{c}}{c}
       \bigg)
                                                                              \nonumber \\
%       
                                                                              \nonumber \\
%       
       \tensor{\dot{\chi}}{_a^a}
       &= 2 \tpartial_t \bigg(
           \frac{\dot{a}}{a} + \frac{\dot{b}}{b} + \frac{\dot{c}}{c}
       \bigg)
                                                                              \nonumber \\
%       
       &= 2 \bigg\{
          \bigg[
              \frac{\ddot{a}}{a} - \bigg(\frac{\dot{a}}{a}\bigg)^2
          \bigg] +
          \bigg[
              \frac{\ddot{b}}{b} - \bigg(\frac{\dot{b}}{b}\bigg)^2
          \bigg] +
          \bigg[
              \frac{\ddot{c}}{c} - \bigg(\frac{\dot{c}}{c}\bigg)^2
          \bigg]
          \bigg\}
                                                                              \nonumber \\
%       
                                                                              \nonumber \\
%
        \tchi[_a^b] \tchi[_b^a]
        &= \tchi[_a^1] \tchi[_1^a] + \tchi[_a^2] \tchi[_2^a] + \tchi[_a^3] \tchi[_3^a]
                                                                              \nonumber \\
%
        &= (\tchi[_1^1])^2 + (\tchi[_2^2])^2 + (\tchi[_3^3])^2
                                                                              \nonumber \\
%
        &= \left( 2 \frac{\dot{a}}{a} \right)^2 +
           \left( 2 \frac{\dot{b}}{b} \right)^2 +
           \left( 2 \frac{\dot{c}}{c} \right)^2
                                                                              \nonumber
   \end{align}


   \begin{align}
       \rightarrow
       \frac{1}{2} \tensor{\dot{\chi}}{_a^a} 
      &+ \frac{1}{4} \tchi[_a^b] \tchi[_b^a]  = 0                             \nonumber \\
%
       \frac{1}{2}
          2 \bigg\{
          \bigg[
              \frac{\ddot{a}}{a} - \cancel{\bigg(\frac{\dot{a}}{a}\bigg)^2}
          \bigg] +
          \bigg[
              \frac{\ddot{b}}{b} - \cancel{\bigg(\frac{\dot{b}}{b}\bigg)^2}
          \bigg] &+
          \bigg[
              \frac{\ddot{c}}{c} - \cancel{\bigg(\frac{\dot{c}}{c}\bigg)^2}
          \bigg]
          \bigg\}
       + \frac{1}{4} 4
           \cancel{\left( \frac{\dot{a}}{a} \right)^2} +
           \cancel{\left( \frac{\dot{b}}{b} \right)^2} +
           \cancel{\left( \frac{\dot{c}}{c} \right)^2}
       = 0
                                                                              \nonumber \\
%
                                                                              \nonumber \\
%
       \therefore
       \frac{\ddot{a}}{a} &+ 
       \frac{\ddot{b}}{b} +
       \frac{\ddot{c}}{c} = 0
       \label{bkl:Einst00}
   \end{align}

   · Termo ${ }_0^k$

   \begin{align}
       0 = 0
       \label{bkl:Einst0k}
   \end{align}


   · Termo ${ }_a^b$



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ ---------- // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
