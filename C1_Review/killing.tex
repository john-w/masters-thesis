%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- KILLING VECTORS -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
    Usually, the treatment of the highly non-linear Einstein field equations
    \eqref{geodiff:EinsteinFieldEquations} is very difficult or even
    analytically impossible for many problems, so it is indispensable that we
    formulate new ways and tools to help us to gain some insight and make our computation
    much less distressing and efficient.

    \

    One such way is to represent the fundamental quantities in the local N-Tuple frame
    discussed in the previous section. Another interesting solution is to look up at the
    \emph{symmetries} of the spacetime carried out by the metric tensor, where the so-called
    \emph{Killing fields} naturally emerge and are the fundamental objects in this
    description, responsible for carring out said symmetries. In order to accomplish such
    task, we shall borrow the already well established (infinitesimal) local approach from 
    field theory.

    \

    We highlight that despite of the seemingly ideal aspects enclosed in the motivation
    above, we have many empirical data that our Universe has a remarkable degree of
    symmetry regarding its spatial sector, thus justifying the present treatment.

    \

    To do that we must construct a covariant toolbox that does not depend on a particular
    choice of a frame of reference. So, we start by defining the \emph{isometry} of the
    metric tensor $g_{\mu\nu}$ as a transformation of coordinates that leaves its functional
    form intact. If we take a diffeomorphism of the metric tensor to another system 
    of coordinates $x \rightarrow x'$,

    \begin{align}
        g_{\mu\nu}(x) = \dpar{x'^\alpha}{x^\mu} \dpar{x'^\beta}{x^\nu} g'_{\alpha\beta}(x')
                                                                    \label{kil:metricDifeom}
                                                                    \,.
    \end{align}

    \noindent
    the isometry condition will be given by

    \begin{align}
        g'_{\alpha\beta}(x') = g_{\alpha\beta}(x') \,, \qquad \forall x'
                                                                        \label{kil:isometry}
                                                                        \..
    \end{align}

    Metric tensors (and tensors in general) that satisfy this condition are also called
    \emph{form invariant}. Since isometries inherently represents some kind of symmetry of
    the spacetime itself, it must be associated to some symmetry group parametrized by, 
    say, $\varepsilon$. That allows us to exploit the formalism of small pertubations around 
    the point%
    \footnote{ A more careful and precise construction can be done if we define the called
               \emph{Lie Transport}, an operation that drags geometric objects along 
               symmetry directions. These linear approximations and the next results
               follows naturally in this formalism.
    }. Then, by considering a transformation of coordinates to the immediate 
    neighbouring of $x$,

    \begin{align*}
        x \rightarrow x' = x + \delta x
        \,,
    \end{align*}

    \noindent
    and setting a pertubation $\delta x$ to the group transformation along with its 
    parameter,

    \simbolo{$\delta x$}{Pertubation}

    \begin{align*}
        \delta x = \varepsilon \xi(x)
        \,,
    \end{align*}

    \noindent
    where $|\varepsilon| \ll 1$, we can describe the infinitesimal coordinate
    transformation by

    \begin{align}
        x^\mu \rightarrow x'^\mu = x^\mu + \varepsilon \xi^\mu(x)
        \,, \qquad |\varepsilon| \ll 1
                                                          \label{kil:infinitesimalTransform}
                                                          \..
    \end{align}

    \simbolo{$\xi^\mu(x)$}{Killing vector}

    Now, we plug \eqref{kil:infinitesimalTransform} into \eqref{kil:metricDifeom} to obtain
    
    \begin{align}
        g_{\mu\nu}(x) &=
        \bigg(\delta^\alpha_\mu + \varepsilon \txi[^\alpha_{,\mu}](x')\bigg)
        \bigg(\delta^\beta_\nu  + \varepsilon \txi[^\beta_{,\nu}](x') \bigg)\ 
            g'_{\alpha\beta}(x')
\iftoggle{steps}{
%            
        \marginnote{
        \begin{align*}
            g'_{\mu\nu} &(x' = x + \varepsilon \xi)
            \\
            &= g'_{\mu\nu}(x) + \varepsilon \xi^\alpha(x) g'_{\mu\nu,\alpha}(x) +
                \mathcal{O}(\varepsilon^2)
        \end{align*}
        }
%
                                                                                \nonumber \\
        &= 
        g'_{\mu\nu}(x') + \varepsilon 
        \bigg(
            g'_{\mu\alpha}(x') \txi[^\alpha_{,\nu}](x')
            + g'_{\alpha\nu}(x') \txi[^\alpha_{,\mu}](x')
        \bigg)
        + \mathcal{O}(\varepsilon^2)
}
                                                                                \nonumber \\
        &=
        g'_{\mu\nu}(x) + \varepsilon \xi^\alpha(x) g'_{\mu\nu,\alpha}(x)
                                                                                \nonumber \\
                                                                                &\qquad
            + \varepsilon \bigg(
                                g'_{\mu\alpha}(x) \txi[^\alpha_{,\nu}](x)
                                + g'_{\alpha\nu}(x) \txi[^\alpha_{,\mu}](x)
                          \bigg)
                                                                                \nonumber
    \end{align}

    \begin{align}
        \rightarrow
        \mathcal{L}_\xi[g_{\mu\nu}] 
        &=
        \txi[^\alpha] \tg[_{\mu\nu,\alpha}] +
        \tg[_{\alpha\nu}] \txi[^\alpha_{,\mu}] +
        \tg[_{\mu\alpha}] \txi[^\alpha_{,\nu}]
                                                                                \nonumber
                                                                                \,,
    \end{align}

    \simbolo{$\mathcal{L}_\xi[\ ]$}{Lie derivative with respect to $\xi$}

    \noindent
    where we have made several expansions in Taylor series, discarted the terms of order
    $\mathcal{O}(\varepsilon^2)$ and defined the \emph{Lie derivative} of a 
    tensor $T$:

    \begin{align}
        \mathcal{L}_\xi[T] 
        &=
        \lim_{\varepsilon \rightarrow 0}
        \frac{T(x) - T'(x)}{\varepsilon}
                                                                   \label{kil:lieDerivative}
                                                                   \,,
    \end{align}

    \noindent
    which quantifies how ``different'' the tensor is in a point immeately next to $x$. This
    operation maps the spacetime back into itself 
    $\mathcal{L}_\xi : \mathcal{M} \rightarrow \mathcal{M}$, thus defining an automorphism,
    and respecting the Leibniz rule. Indeed, if $T$ is a tensor of rank $(0, m+n)$ whose
    components are

    \begin{align*}
        \tT[_{\alpha_1 \ldots \alpha_m \beta_1 \ldots \beta_n}]
        =
        f \tA[_{\alpha_1 \ldots \alpha_m}] \tB[_{\beta_1 \ldots \beta_n}]
        \,,
    \end{align*}

    \noindent
    we have

    \begin{align}
        \mathcal{L}_\xi[ \tT[_{\alpha_1 \ldots \alpha_m \beta_1 \ldots \beta_n}] ]
        &=
        \mathcal{L}_\xi[ f \tA[_{\alpha_1 \ldots \alpha_m}] \tB[_{\beta_1 \ldots \beta_n}]]
                                                                                \nonumber \\
        &=
        \xi^\lambda
        \tT[_{\alpha_1 \ldots \alpha_m \beta_1 \ldots \beta_n,\lambda}]
                                                                                \nonumber \\
                                                                                &\qquad
        +
        \tT[_{\lambda \ldots \alpha_m \beta_1 \ldots \beta_n}]
        \txi[^\lambda_{,{\alpha_1}}]
        + \ldots
                                                                                \nonumber \\
                                                                              &\qquad \qquad
        + \ldots + \tT[_{\alpha_1 \ldots \lambda \beta_1 \ldots \beta_n}]
        \txi[^\lambda_{,{\alpha_m}}]
                                                                                \nonumber \\
                                                                                &\qquad
        + \tT[_{\alpha_1 \ldots \alpha_m \lambda \ldots \beta_n}]
        \txi[^\lambda_{,{\beta_1}}]
        + \ldots
                                                                                \nonumber \\
                                                                              &\qquad \qquad
        + \ldots + \tT[_{\alpha_1 \ldots \alpha_m \beta_1 \ldots \lambda}]
        \txi[^\lambda_{,{\beta_n}}]
                                                                                \nonumber \\
                                                                                \nonumber \\
        &=
        \xi^\lambda
        (f \tA[_{\alpha_1 \ldots \alpha_m}] \tB[_{\beta_1 \ldots \beta_n}])_{,\lambda}
                                                                                \nonumber \\
                                                                                &\qquad
        + f \tA[_{\lambda \ldots \alpha_m}] \tB[_{\beta_1 \ldots \beta_n}]
        \txi[^\lambda_{,{\alpha_1}}]
        + \ldots
                                                                                \nonumber \\
                                                                              &\qquad \qquad
        + \ldots + f \tA[_{\alpha_1 \ldots \lambda}] \tB[_{\beta_1 \ldots \beta_n}]
        \txi[^\lambda_{,{\alpha_m}}]
                                                                                \nonumber \\
                                                                                &\qquad
        + f \tA[_{\alpha_1 \ldots \alpha_m}] \tB[_{\lambda \ldots \beta_n}]
        \txi[^\lambda_{,{\beta_1}}]
        + \ldots
                                                                                \nonumber \\
                                                                              &\qquad \qquad
        + \ldots + f \tA[_{\alpha_1 \ldots \alpha_m}] \tB[_{\beta_1 \ldots \lambda}]
        \txi[^\lambda_{,{\beta_n}}]
                                                                                \nonumber \\
                                                                                \nonumber \\
        &=
        \big( \xi^\lambda f_{,\lambda} \big) 
            \tA[_{\alpha_1 \ldots \alpha_m}] \tB[_{\beta_1 \ldots \beta_n}]
                                                                                \nonumber \\
                                                                                &\qquad
            + f \tB[_{\beta_1 \ldots \beta_n}]
            \big(
                \xi^\lambda \tA[_{\alpha_1 \ldots \alpha_m,\lambda}] +
                                                                                \nonumber \\
                                                                              &\qquad \qquad
                + \tA[_{\lambda \ldots \alpha_m}] \txi[^\lambda_{,{\alpha_1}}]
                + \ldots
                                                                                \nonumber \\
                                                                              &\qquad \qquad
                + \ldots + \tA[_{\alpha_1 \ldots \lambda}] \txi[^\lambda_{,{\alpha_m}}]
            \big)
                                                                                \nonumber \\
                                                                                &\qquad
            + f \tA[_{\alpha_1 \ldots \alpha_m}]
            \big(
                \xi^\lambda \tB[_{\beta_1 \ldots \beta_n,\lambda}] +
                                                                                \nonumber \\
                                                                              &\qquad \qquad
                + \tB[_{\lambda \ldots \beta_n}] \txi[^\lambda_{,{\beta_1}}]
                + \ldots
                                                                                \nonumber \\
                                                                              &\qquad \qquad
                + \ldots + \tB[_{\beta_1 \ldots \lambda}] \txi[^\lambda_{,{\beta_n}}]
            \big)
                                                                                \nonumber \\
                                                                                \nonumber \\
        \begin{split}
        &=
        \mathcal{L}_\xi[f] \tA[_{\alpha_1 \ldots \alpha_m}] \tB[_{\beta_1 \ldots \beta_n}]
                                                                                 \\
                                                                              &\qquad \qquad
        +
        f \mathcal{L}_\xi[\tA[_{\alpha_1 \ldots \alpha_m}]] \tB[_{\beta_1 \ldots \beta_n}]
                                                                                 \\
                                                                              &\qquad \qquad
        +
        f \tA[_{\alpha_1 \ldots \alpha_m}] \mathcal{L}_\xi[\tB[_{\beta_1 \ldots \beta_n}]]
        \end{split}
                                                       \label{kil:LieDerivativeLeibinizRule}
                                                       \..
    \end{align}

    \

    Immediately, the isometry condition \eqref{kil:isometry} is codified such that the Lie
    derivative of the metric tensor vanishes

    \begin{align}
        \mathcal{L}_\xi[g_{\mu\nu}] = 0
                                                           \label{kil:isometryLieDerivative}
                                                           \..
    \end{align}

    Therefore, the \emph{Killing conditions} are

    \begin{align}
        \txi[^\alpha] \tg[_{\mu\nu,\alpha}] +
        \tg[_{\alpha\nu}] \txi[^\alpha_{,\mu}] +
        \tg[_{\mu\alpha}] \txi[^\alpha_{,\nu}]
        = 0
                                                          \label{kil:killingConditionContra}
                                                          \,,
    \end{align}

    \noindent
    or, in the covariant form,

\iftoggle{steps}{        
    \begin{align}
        0 &=
        \txi[^\alpha] \tg[_{\mu\nu,\alpha}] +
        \bigg(
            (\tg[_{\alpha\nu}] \txi[^\alpha])_{,\mu} 
            - \tg[_{\alpha\nu,\mu}] \txi[^\alpha]      
        \bigg) + 
        \bigg(
            (\tg[_{\mu\alpha}] \txi[^\alpha])_{,\nu} 
            - \tg[_{\mu\alpha,\nu}] \txi[^\alpha]      
        \bigg)
                                                                                \nonumber \\
        &=
        \txi[_{\mu,\nu}] + \txi[_{\nu,\mu}] - \txi[^\alpha]
            \bigg(
                \underbrace{
                \tg[_{\mu\alpha,\nu}] + \tg[_{\alpha\nu,\mu}] - \tg[_{\mu\nu,\alpha}]
                }_{2 \tg[_{\alpha\beta}] \Gamma^\beta_{\mu\nu}}
            \bigg)
                                                                                \nonumber \\
        &=
        \bigg( \txi[_{\mu,\nu}] - \txi[_\alpha] \Gamma^\alpha_{\mu\nu} \bigg) +
        \bigg( \txi[_{\nu,\mu}] - \txi[_\alpha] \Gamma^\alpha_{\nu\mu} \bigg)
                                                                                \nonumber
    \end{align}
}

    \begin{align}
        \therefore
        \txi[_{\mu;\nu}] + \txi[_{\nu;\mu}] = 0
                                                             \label{kil:killingConditionCov}
                                                             \..
    \end{align}

    Any N-vector field $\xi_\mu(x)$ that satisfies \eqref{kil:killingConditionCov} gets the
    name of \emph{Killing fields} or simply \emph{Killing vectors}, which are the generators 
    of the associated symmetry group that carries the symmetries of the space. To determine 
    all the isometries of the spaces, we just need to find all the Killing fields that
    satisfy the condition above. Inasmuch as those Killing fields represent the symmetries 
    of the space, we also expect them to be related to conserved quantities by virtue of
    Noether theorem.

    \

    We pick the case of Killing fields associated with the conservation of the total linear
    momentum

    \subsubsection*{Example:} Linear momentum and associated Killing vector:

    \begin{align*}
        \frac{d}{d \tau} \bigg( \xi^\mu P_\mu \bigg)
        &=
        \xi_\mu \frac{d P^\mu}{d \tau} + \frac{d \xi_\mu}{d \tau} P^\mu
                                                                                          \\
        &=
        \xi_\mu \bigg(
            - \frac{1}{m} \Gamma^\mu_{\alpha\beta} P^\alpha P^\beta
        \bigg) +
        \dpar{\xi_\mu}{x^\nu} \frac{d x^\nu}{d\tau} P^\mu
                                                                                          \\
        &=
        - \xi_\mu \frac{1}{m} \Gamma^\mu_{\alpha\beta} P^\alpha P^\beta
        + \frac{1}{m} \txi[_{\mu,\nu}] P^\nu P^\mu
                                                                                          \\
        &=
        \frac{1}{m} \bigg(
            \txi[_{\mu,\nu}] - \Gamma^\alpha_{\mu\nu} \txi[_\alpha]
        \bigg) P^\mu P^\nu
                                                                                          \\
        &=
        \frac{1}{m} \underbrace{ \txi[_{\mu;\nu}] 
                    }_\text{anti-sym}
                    \underbrace{ P^\mu P^\nu 
                    }_\text{sym} 
                                                                                          \\
        &= 0
    \end{align*}

    \begin{align*}
        \therefore \xi_\mu P^\mu = \text{const}
        \,,
    \end{align*}

    \noindent
    where we made use of the Geodesic equation \eqref{geodiff:geodesicEquation},

    \noindent
    $\dfrac{d P^\mu}{d\tau} + \frac{1}{m} \Gamma^\mu_{\alpha\beta} P^\alpha P^\beta = 0$.
    $\blacksquare$

    \

    In order to relate the Killing vectors to geometry, we will explicitly relate them with
    the curvature tensor by using \eqref{geodiff:RiemannCurvatureTensorByCommutator} and, 
    employing the first Bianchi identities \eqref{geodiff:FirstBianchiIdentity}, simplify the 
    results.  More specifically, applying \eqref{geodiff:RiemannCurvatureTensorByCommutator} 
    to $\xi_\rho$ on $[\nabla_\mu, \nabla_\nu]$ and summing the results cyclically yields

    \begin{align}
\iftoggle{steps}{        
        \bigg(
            \underbrace{
            \tR[^\alpha_{\rho\mu\nu}] + 
            \tR[^\alpha_{\mu\nu\rho}] + 
            \tR[^\alpha_{\nu\rho\mu}]
            }_{= 0}
        \bigg) \xi_\alpha
        &=
        \bigg( \txi[_{\rho;\mu;\nu}] - \txi[_{\rho;\nu;\mu}] \bigg) +
        \bigg( \txi[_{\mu;\nu;\rho}] - \underbrace{ \txi[_{\mu;\rho;\nu}] 
                                       }_{ - \txi[_{\rho;\mu;\nu}] }\bigg) +
        \bigg( \underbrace{ \txi[_{\nu;\rho;\mu}] 
               }_{ -\txi[_{\rho;\nu;\mu}] } - \underbrace{ \txi[_{\nu;\mu;\rho}] 
                                              }_{ -\txi[_{\mu;\nu;\rho}] } \bigg)
                                                                                \nonumber \\
        &=
        2 \txi[_{\rho;\mu;\nu}] + 2 \txi[_{\mu;\nu;\rho}] - 2 \txi[_{\rho;\nu;\mu}]
                                                                                \nonumber \\
                                                                                \nonumber \\
        \implies                                                                        
}                                                                                
        \txi[_{\mu;\nu;\rho}]
        &= \txi[_{\rho;\nu;\mu}] - \txi[_{\rho;\mu;\nu}]
                                                                                \nonumber \\
        &=
        \tR[^\lambda_{\rho\nu\mu}] \xi_\lambda
                                                                     \label{kil:covcovDeriv}
                                                                     \..
    \end{align}

    In other words, \emph{only the Killing vectors and their first covariant derivatives
    are independent}; further covariant derivatives will always be expressed in terms of
    $\xi_\mu$ and $\xi_{\mu;\nu}$ only. Though extremely restritive, we can use this fact
    for our benefit. For instance, if we know the values $\xi_\mu$ and $\xi_{\mu;\nu}$
    takes at some point $x_0$, we can express \emph{any} Killing vector as a Taylor series 
    around $x_0$

    \begin{align}
        \txi[^n_\mu](x, x_0)
        &=
\iftoggle{steps}{        
        \txi[^n_\mu](x_0) + \txi[^n_{\mu,\nu}](x_0) (x^\nu - x_0^\nu) + 
            \mathcal{O}\bigg( (x-x_0)^2 \bigg)
                                                                                \nonumber \\
        &=
        \txi[^n_\mu](x_0) + 
        \bigg( 
            \txi[^n_{\mu;\nu}](x_0) + \Gamma^\alpha_{\mu\nu}(x_0) \txi[_\alpha](x_0)
        \bigg) (x^\nu - x_0^\nu) +
            \mathcal{O}\bigg( (x-x_0)^2 \bigg)
                                                                                \nonumber \\
        &=
        \bigg(
            \delta^\alpha_\mu + \Gamma^\alpha_{\mu\nu}(x_0) (x^\nu - x_0^\nu)
        \bigg) \txi[^n_\alpha](x_0) + 
        \txi[^n_{\alpha;\nu}](x_0) \delta^\alpha_\mu (x^\nu - x_0^\nu) + 
            \mathcal{O}\bigg( (x-x_0)^2 \bigg)
                                                                                \nonumber \\
                                                                                \nonumber \\
        &=
}
        \tA[_\mu^\lambda](x,x_0) \txi[^n_\lambda] (x_0) +
        \tB[_\mu^{\lambda\sigma}](x,x_0) \txi[^n_{\lambda;\sigma}] (x_0)
                                                          \label{kil:killingVectorExpansion}
                                                          \,,
    \end{align}

    \noindent
    where $n$ labels one of the $N$ vectors $\xi_\mu$ or one of the $\frac{1}{2}N(N-1)$
    derivatives $\xi_{\mu;\nu}$ in the spacetime and the coefficients
    $\tA[_\mu^\lambda]$ and $\tB[_\mu^{\lambda\sigma}]$ depend upon $x_0$ and the metric
    $g_{\mu\nu}$ in some way but \emph{does not} depend on the Killing vectors 
    $\xi_\lambda(x_0)$ and $\xi_{\lambda;\sigma}(x_0)$, so they are the same for any and
    \emph{all} Killing vector.

    \

    For \eqref{kil:covcovDeriv} to be soluble, it needs to satisfy integrability conditions, 
    which can be found when we take the commutator of covariant derivatives, but now of 
    $\xi_{\rho;\mu}$. Using \eqref{kil:covcovDeriv},

    \begin{align}
        \tR[^\lambda_{\rho\sigma\nu}] \txi[_{\lambda;\mu}] 
        + \tR[^\lambda_{\mu\sigma\nu}] \overbrace{ \txi[_{\rho;\lambda}] 
                                                 }^{-\txi[_{\lambda;\rho}]}
        &= \txi[_{\rho;\mu;\sigma;\nu}] - \txi[_{\rho;\mu;\nu;\sigma}]
                                                                                \nonumber \\
\iftoggle{steps}{        
        { }^\eqref{kil:covcovDeriv}
        &= 
        ( \tR[^\lambda_{\sigma\mu\rho}] \txi[_{\lambda}] )_{;\nu} - 
        ( \tR[^\lambda_{\nu\mu\rho}] \txi[_{\lambda}] )_{;\sigma}
                                                                                \nonumber \\
        &= 
        ( \tR[^\lambda_{\sigma\mu\rho;\nu}]
          - \tR[^\lambda_{\nu\mu\rho;\sigma}] )\ \txi[_\lambda]
        + \tR[^\lambda_{\sigma\mu\rho}] \txi[_{\lambda;\nu}]
        - \tR[^\lambda_{\nu\mu\rho}] \txi[_{\lambda;\sigma}]
                                                                                \nonumber \\
                                                                                \nonumber \\
        \tR[^\lambda_{\rho\sigma\nu}] \txi[_{\lambda;\mu}] 
        - \tR[^\lambda_{\mu\sigma\nu}] \txi[_{\lambda;\rho}] 
        + \tR[^\lambda_{\nu\mu\rho}] \txi[_{\lambda;\sigma}]
        - \tR[^\lambda_{\sigma\mu\rho}] \txi[_{\lambda;\nu}]
        &= 
        (  
        \tR[^\lambda_{\sigma\mu\rho;\nu}] 
        - \tR[^\lambda_{\nu\mu\rho;\sigma}]
        )\ \txi[_\lambda]
                                                                                \nonumber \\
                                                                                \nonumber \\
}
        \therefore
        \bigg(
        - \tR[^\lambda_{\rho\sigma\nu}] \delta^\kappa_\mu
        + \tR[^\lambda_{\mu\sigma\nu}]  \delta^\kappa_\rho
       &- \tR[^\lambda_{\nu\mu\rho}]    \delta^\kappa_\sigma
        + \tR[^\lambda_{\sigma\mu\rho}] \delta^\kappa_\nu
        \bigg)\ \txi[_{\lambda;\kappa}]
                                                                                \nonumber \\
                                                                               &\qquad\qquad
        = 
        ( \tR[^\lambda_{\nu\mu\rho;\sigma}] 
        - \tR[^\lambda_{\sigma\mu\rho;\nu}] )\ \txi[_\lambda]
                                                            \label{kil:killingIntegrability}
                                                            \..
    \end{align}

    This is yet another restrictive condition, but now it establishes a link between the
    Killing vectors and their first covariant derivative.

    
    \fancyend

}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ --------------- // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
