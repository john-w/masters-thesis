%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- PARTICULAR RESULTS -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
    In this section we shall enumerate some more handy results that we shall be using further
    throughout the work but are too specific to deserve a whole section to
    elaborate them.

\subsection{Proper time}
\label{ssec:properTime}
{{{
    The \emph{proper time} of a physical entity is defined as the time measured by clocks
    in the frame of reference of that entity or, equivalently, by the referential where
    this object is stationary. Thus, there are no spatial variations and the line element
    reduces to%
    \footnote{From now on we shall adopt $c=1$.}

    \begin{align*}
        ds^2 = dt^2
        \,,
    \end{align*}

    \noindent
    which can be inverted as

    \begin{align*}
        d\tau \equiv dt = \sqrt{ds^2}
    \end{align*}

    \noindent 
    or, in a path from $A$ to $B$, considering $\tau_A \equiv 0$ and $\tau_B \equiv \tau$,

    \begin{align}
        \tau = \int_A^B \sqrt{ds^2}
                                                                      \label{par:properTime}
                                                                      \..
    \end{align}

    \noindent
    We thus obtain a description that clearly is covariant, since $ds^2$ is so. For this 
    reason, we distinguish it by using the symbol $\tau$.

    \simbolo{$\tau$}{Proper time}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=.3\textwidth] {res/Var__ProperTime.pdf}
        \captionof{figure}{Proper time representation in a Minkowski diagram. It is the 
            time a clock displays in a frame of reference that moves along with the 
            particle.}
        \label{fig:var:properTime}
    \end{figure}



}}}

\subsection{Synchronous Frame of Reference}
\label{ssec:syncRef}
{{{
    This frame of reference is characterized by a family of hypersurfaces in which all the 
    ``clocks'' carried by the spatial points of every sheet are synchronized among 
    themselves. By virtue of that, the hypersufaces are in free-fall, so they follow
    geodesic trajectories and have the proper time $\tau$ as their time coordinate; 
    we can say that time and spatial coordinates do not mix up, effectively given the
    following spacetime interval

    \begin{align}
        ds^2 
        &= g_{00} d\tau^2 + g_{ij} dx^i dx^j
                                                                                \nonumber \\
        &= d\tau^2 + g_{ij} dx^i dx^j
                                                                       \label{sync:interval}
                                                                       \,,
    \end{align}

    \noindent
    where a simple redefinition of coordinates allows us to set $g_{00} \equiv 1$. From
    that line element, we infer that the metric tensor has the components

    \begin{align}
        \tg[_0_0] = 1
        \qquad \,; \qquad
        \tg[_0_i] = 0  
        \qquad \,; \qquad
        \tg[_i_j] \equiv - \zeta_{ij}
                                                                     \label{sync:syncMetric}
                                                                     \,,
    \end{align}

    \noindent
    in which we define the positively defined \emph{spatial metric tensor} $\zeta$ of the
    $N-1$ dimensional spatial sector of the space. By definition, the normal N-vector for
    the hypersurfaces with $\tau =$ const is

    \simbolo{$\zeta_{ij}$}{Purely spatial metric tensor}
    \simbolo{$n_\mu$}{Normal N-vector}

    \begin{align*}
        n_\mu = \dpar{\tau}{x^\mu}
    \end{align*}

    \noindent
    which gives

    \begin{align}
        n_\mu = n^\mu &= (1, \bs{0})
                                                                  \label{sync:normalNvector}
                                                                  \,,
    \end{align}

    \noindent
    already properly normalized; both co- and contravariant representations are identical
    by \eqref{sync:syncMetric}.

    \

    In this frame of reference the N-velocities

    \begin{align}
        u^\mu := \dpar{x^\mu}{s}
    \end{align}

    \noindent
    point to the time direction, so they are all tangent to $x^i =$ const, coinciding with
    the normal $N$-vector 

    \begin{align}
        u^\mu = (1, \bs{0})
                                                                    \label{sync:Nvelocities}
                                                                    \,,
    \end{align}

    \noindent
    and thus following geodesic curves. Indeed, the geodesic equation
    \eqref{geodiff:geodesicEquation} is automatically satisfied, for

    \begin{align*}
        \tGamma[^\mu_0_0] &= \frac{1}{2} \tg[^\mu^\sigma] 
                             \big(
                                \tg[_0_\sigma_{,0}] + 
                                \tg[_\sigma_0_{,0}] - 
                                \tg[_0_0_{,\sigma}] 
                             \big)                                                        \\
                          &\equiv 0
                          \ \,; \quad \forall \mu \,,
    \end{align*}

    \begin{align*}
        \frac{d u^\mu}{d\tau} + \tGamma[^\mu_\alpha_\beta] u^\alpha u^\beta = 0
    \end{align*}

    \begin{align*}
        \implies
        \frac{d u^\mu}{d\tau} = 0
        \,,
    \end{align*}

    \noindent
    which is identically satisfied by $n_\mu$ in \eqref{sync:Nvelocities}.

    \

    \begin{figure}[h!]
        \centering
        \includegraphics[width=.4\textwidth]{res/Part__SyncSpaces.pdf}
        \captionof{figure}{Synchronous frame of reference spatial foliation.}
        \label{fig:sync:spacetimeFoliation}
    \end{figure}

    \

    Enumerating all qualities that a synchronous frame of reference must have, we can then
    \emph{always} construct this frame for any spacetime if we erect it employing the 
    following methodology:

    \begin{enumerate}[i.]
        \item Choose a starting everywhere space-like hypersurface $\Sigma_i$ such that
              the normals $n_\mu$ point to the time direction;
        \item Derive the geodesic curves normal to $\Sigma_i$;
        \item Set the time coordinate as the geodesic length $s$ of those curves measured
              from $\Sigma_i$.
    \end{enumerate}

    It is important to remark that there are infinite ways of defining a syncronous frame
    of reference thanks to the everywhere spatial subspace described by $\zeta_{ij}$; any
    coordinate transformation of it will return another syncronous frame.

    \

    To write the Einstein equations in this frame of reference, it is useful to split the
    time and space components up, in particular defining the time derivative of
    $\zeta$ as\footnote{The last one is easily proved by using the identity
    $\log(det A) = tr(\log A)$.}:

    \begin{subequations}
    \begin{align}
    \begin{split}
        \chi_{ab} &:=   \partial_t \zeta_{ab} = - g_{ab,0}
    \end{split}
    \,,
                                                                     \label{sync:syncChi} \\
    \begin{split}
        \chi_{ab} &= \chi_{ba}                                            
    \end{split}
    \,,
                                                                    \label{sync:symmetry} \\
    \begin{split}
        \tchi[_a^a] &= \tzeta[^{ab}] \partial_t
                       \tzeta[_{ab}]
                     = \partial_t \ln \zeta
    \end{split}
    \,,
                                                                \label{sync:syncChiTrace}
    \end{align}
                                                                    \label{sync:syncChis}
    \end{subequations}

    \simbolo{$\chi_{ab}$}{Temporal derivative of $\zeta_{ab}$}

    \noindent
    where operations of lowering/raising indices are carried out by $\zeta_{ij}$. The
    Christoffel symbols take the form

    \begin{align}
        \tGamma[^0_{00}] = 
        \tGamma[^0_{0i}] &= \tGamma[^i_{00}] = 0 
                                                                                \ \ ;
                                                                                \nonumber \\
        \tGamma[^0_i_j]  = \frac{1}{2} \tchi[_i_j] 
                                                                                \ \ ; \ \
        \tGamma[^i_0_j] &= \frac{1}{2} \tchi[^i_j] 
                                                                                \ \ ; \ \
        \tGamma[^i_j_k] := \tLambda[^i_j_k]
                                                              \label{sync:syncChristofel}
                                                              \,.
    \end{align}

    \noindent
    where the last one are the purely spatial symbols formed by $\zeta_{ij}$. With that,
    we are able to adress the Ricci tensor \eqref{geodiff:RicciTensor},


    \begin{align*}
        \tR[_\mu_\nu] 
        &= 
        \tGamma[^\alpha_{\mu\nu,\alpha}] 
               - \tGamma[^\alpha_{\mu\alpha,\nu}] 
               - \tGamma[^\varepsilon_{\mu\alpha}] 
                        \tGamma[^\alpha_{\varepsilon\nu}] 
               + \tGamma[^\varepsilon_{\mu\nu}] 
                                         \tGamma[^\alpha_{\varepsilon\alpha}] 
        \,,
    \end{align*}

    \noindent
    in this coordinate system. %
%
\iftoggle{steps}{
    \

                                     %  * * *  %
                                     \fancybreak
                                     \vspace{-.3in}
                                     %  * * *  %

    %%%%%%%%%%%%%%
    % R_00 %%%%%%%
    %%%%%%%%%%%%%%
    \begin{align*}
        \rightarrow \tR[_0_0] 
              &=    \cancelto{0}{\tGamma[^\alpha_{00,\alpha}]}
                  - \tGamma[^\alpha_{0\alpha,0}] 
                  - \tGamma[^\varepsilon_{0\alpha}] 
                           \tGamma[^\alpha_{\varepsilon0}] 
                  + \cancelto{0}{\tGamma[^\varepsilon_{00}]}
                           \tGamma[^\alpha_{\varepsilon\alpha}]                           \\
%                           
              &= - ( \cancelto{0}{\tGamma[^0_{00,0}]} + 
                   \tGamma[^i_{0i,0}] ) -
                 ( \cancelto{0}{\tGamma[^0_{0\alpha}]} 
                           \tGamma[^\alpha_{00}] +
                   \tGamma[^i_{0\alpha}] 
                           \tGamma[^\alpha_{i0}] )                                        \\
%                           
              &= -\frac{1}{2} \partial_t \tchi[^i_i] -
                 ( \cancelto{0}{\tGamma[^i_{00}]}
                           \tGamma[^0_{i0}] +
                   \tGamma[^i_{0j}] 
                           \tGamma[^j_{i0}] )                                             \\
%                           
              &= -\frac{1}{2} \partial_t \tchi[^i_i] -
              \frac{1}{2} \tchi[^i_j]
                          \frac{1}{2} \tchi[^j_i]                                    \\ \ \\
%                          
        \therefore \tR[_0_0]
              &= -\frac{1}{2} \partial_t \tchi[^i_i] -
              \frac{1}{4} \tchi[^i_j]
                          \tchi[^j_i]
    \end{align*}

    %%%%%%%%%%%%%%
    % R_0k %%%%%%%
    %%%%%%%%%%%%%%
    \begin{align*}
        \rightarrow \tR[_0_k] 
              &=    \tGamma[^\alpha_{0k,\alpha}]
                  - \tGamma[^\alpha_{0\alpha,k}]
                  - \tGamma[^\varepsilon_{0\alpha}] 
                           \tGamma[^\alpha_{\varepsilon k}] 
                  + \tGamma[^\varepsilon_{0k}]
                           \tGamma[^\alpha_{\varepsilon\alpha}]                           \\
%                           
              &=    (
                      \cancelto{0}{\tGamma[^0_{0k,0}]} 
                    + \tGamma[^i_{0k,i}]
                    )
                  - (
                      \cancelto{0}{\tGamma[^0_{00,k}]} 
                    + \tGamma[^i_{0i,k}]
                    )
                  - (
                      \cancelto{0}{\tGamma[^0_{0\alpha}]}
                           \tGamma[^\alpha_{0k}] 
                    + \tGamma[^i_{0\alpha}] 
                           \tGamma[^\alpha_{ik}] 
                    )
                  + (
                      \cancelto{0}{\tGamma[^0_{0k}]}
                           \tGamma[^\alpha_{0\alpha}] 
                    + \tGamma[^i_{0k}]
                           \tGamma[^\alpha_{i\alpha}]
                    )                                                                     \\
%
              &=    \tGamma[^i_{0k,i}]
                  - \tGamma[^i_{0i,k}]
                  - (
                      \cancelto{0}{\tGamma[^i_{00}]}
                           \tGamma[^0_{ik}] 
                    + \tGamma[^i_{0j}] 
                           \tGamma[^j_{ik}] 
                    )
                  + (
                      \tGamma[^i_{0k}]
                           \cancelto{0}{\tGamma[^0_{i0}]} 
                    + \tGamma[^i_{0k}]
                           \tGamma[^j_{ij}]
                    )                                                                     \\
%
              &=    \frac{1}{2} \tchi[^i_{k,i}]
                  - \frac{1}{2} \tchi[^i_{i,k}]
                  - \frac{1}{2} \tchi[^i_j]
                           \tGamma[^j_{ik}] 
                  + \frac{1}{2} \tchi[^i_k]
                           \tGamma[^j_{ij}]                                 
                  - \underbrace{
               \left( \frac{1}{2} \tGamma[^j_k_i] \tchi[^i_j]  
                    - \frac{1}{2} \tGamma[^j_i_k] \tchi[^i_j] \right)
                    }_{\text{adding 0}}                                                   \\
%
              &=  \frac{1}{2} \left[  
                  \underbrace{  \left(  
                      \tchi[^i_{k,i}] +
                      \tGamma[^j_{ji}] \tchi[^i_k] 
                    - \tGamma[^j_{ki}] \tchi[^i_j]
                  \right)}_{\tchi[^i_{k;i}]} 
                  - \underbrace{ \left(  
                      \tchi[^i_{i,k}] 
                    + \tGamma[^j_{ik}] \tchi[^i_j] 
                    - \tGamma[^j_{ik}] \tchi[^i_j]
                  \right)}_{\tchi[^i_{i;k}]}
                  \right]                                                                 \\
%
        \therefore \tR[_0_k]
              &= \frac{1}{2} \left( 
                      \tchi[^i_{k;i}] - \tchi[^i_{i;k}]
              \right)
    \end{align*}

    %%%%%%%%%%%%%%
    % R_0k %%%%%%%
    %%%%%%%%%%%%%%
    \begin{align*}
        \rightarrow \tR[_i_j] 
              &=    \tGamma[^\alpha_{ij,\alpha}]
                  - \tGamma[^\alpha_{i\alpha,j}] 
                  - \tGamma[^\varepsilon_{i\alpha}] 
                           \tGamma[^\alpha_{\varepsilon j}] 
                  + \tGamma[^\varepsilon_{ij}]
                           \tGamma[^\alpha_{\varepsilon\alpha}]                           \\
%
              &=    (
                      \tGamma[^0_{ij,0}] 
                    + \tGamma[^k_{ij,k}]
                    )
                  - (
                      \cancelto{0}{\tGamma[^0_{i0,j}]} 
                    + \tGamma[^k_{ik,j}] 
                    )
                  - ( 
                      \tGamma[^0_{i\alpha}] 
                           \tGamma[^\alpha_{0j}] 
                    + \tGamma[^k_{i\alpha}] 
                           \tGamma[^\alpha_{kj}] 
                    )
                  + ( 
                      \tGamma[^0_{ij}]
                           \tGamma[^\alpha_{0\alpha}] 
                    + \tGamma[^k_{ij}]
                           \tGamma[^\alpha_{k\alpha}] 
                    )                                                                     \\
%
              &=    (
                      \tGamma[^0_{ij,0}] 
                     +\tGamma[^k_{ij,k}] 
                     -\tGamma[^k_{ik,j}] 
                    )
                  - [( 
                          \cancelto{0}{
                      \tGamma[^0_{i0}]}
                           \tGamma[^0_{0j}] 
                     +\tGamma[^0_{il}] 
                           \tGamma[^l_{0j}] 
                     ) + (
                      \tGamma[^k_{i0}] 
                           \tGamma[^0_{kj}] 
                     +\tGamma[^k_{il}] 
                           \tGamma[^l_{kj}] 
                     )]
                  + [( 
                      \tGamma[^0_{ij}]
                          \cancelto{0}{
                          \tGamma[^0_{00}]} 
                     +\tGamma[^0_{ij}]
                          \tGamma[^l_{0l}] 
                     ) + (                                                               
                      \tGamma[^k_{ij}]
                          \cancelto{0}{
                          \tGamma[^0_{k0}]} 
                     +\tGamma[^k_{ij}]
                          \tGamma[^l_{kl}] 
                     )]                                                                   \\
%
              &=    (
                      \tGamma[^0_{ij,0}] 
                     -\tGamma[^0_{il}] 
                           \tGamma[^l_{0j}] 
                     -\tGamma[^k_{i0}] 
                           \tGamma[^0_{kj}] 
                     +\tGamma[^0_{ij}]
                          \tGamma[^l_{0l}]
                    )
                  + \underbrace{ ( 
                      \tGamma[^k_{ij,k}] 
                     -\tGamma[^k_{ik,j}]
                     -\tGamma[^k_{il}] 
                           \tGamma[^l_{kj}] 
                     +\tGamma[^k_{ij}]
                          \tGamma[^l_{kl}] 
                     )}_{\equiv \tP[_{ij}]}                                               \\
%
              &=      \tP[_{ij}] 
                    + \frac{1}{2} \partial_t \tchi[_{ij}] 
                    - \left( \frac{1}{2} \tchi[_{il}] \right)
                      \left( \frac{1}{2} \tchi[^l_j]  \right) 
                    - \left( \frac{1}{2} \tchi[^k_i]  \right)
                      \left( \frac{1}{2} \tchi[_{kj}] \right) 
                    + \left( \frac{1}{2} \tchi[_{ij}] \right)
                      \left( \frac{1}{2} \tchi[^l_l]  \right)                             \\
%
    \therefore \tR[_i_j]
              &= \tP[_i_j] + \frac{1}{2} \partial_t \tchi[_i_j] 
                 + \frac{1}{4} \left(
                       \tchi[_i_j] \tchi[^l_l]
                     - 2 \tchi[_i_l] \tchi[^l_j]  
                 \right)
    \end{align*}

    Here we defined the \emph{totally spatial Ricci tensor} $P_{ij}$ as the same built with
    $\zeta_{ij}$ and $\tLambda[^i_{jk}]$.

                                     %  * * *  %
                                     \fancybreak
                                     %  * * *  %
                                     

    Putting everything together

}{
    Doing all the calculations, one gets
}


    \begin{subequations}
    \begin{align}
    \begin{split}
         \tR[_0_0]
              &= - \frac{1}{2} \partial_t \tchi[^i_i] 
                 - \frac{1}{4} \tchi[^i_j] \tchi[^j_i]
    \end{split} \,,                                              \label{sync:syncRicci00} \\
%         
    \begin{split}
         \tR[_0_k]
              &= \frac{1}{2} \left( 
                     \tchi[^i_{k;i}] - \tchi[^i_{i;k}]
              \right)
    \end{split} \,,                                              \label{sync:syncRicci0k} \\
%         
    \begin{split}
         \tR[_i_j]
              &= \tP[_i_j] + \frac{1}{2} \partial_t \tchi[_i_j] 
                 + \frac{1}{4} \left(
                       \tchi[_i_j] \tchi[^l_l]
                     - 2 \tchi[_i_l] \tchi[^l_j]  
              \right)
    \end{split} \,,                                              \label{sync:syncRicciij}
    \end{align}
                                                                   \label{sync:syncRicci}
    \end{subequations}

    \noindent
\iftoggle{steps}{%
    and by \eqref{var:EinsteinHilbertFieldEquationsFull_functionOfT} in
    mixed components 
}{%
    where $P_{ij}$ is the \emph{totally spatial Ricci tensor} built with $\zeta_{ij}$ and
    $\tLambda[^i_{jk}]$. Finally, the Einstein equations 
    \eqref{geodiff:EinsteinTensorSolution} in mixed components reduce to the following
    system of differential equations:
}

    \simbolo{$P_{ij}$}{Purely spatial Ricci tensor}

    \begin{subequations}
    \begin{align}
    \begin{split}
         \tR[_0^0]
              &= \frac{1}{2} \partial_t \tchi[_i^i] 
              + \frac{1}{4} \tchi[^i_j]
                \tchi[^j_i]                              
               = 8\pi G \left( \tT[_0^0] - \frac{1}{2} T \right)
    \end{split} \,,                                              \label{sync:syncEinst00} \\
%    
    \begin{split}
         \tR[_k^0]
              &= \frac{1}{2} \left( 
                     \tchi[^i_{i;k}] - \tchi[^i_{k;i}]
              \right)                                          
              = 8 \pi G \tT[_k^0]
    \end{split} \,,                                              \label{sync:syncEinst0k} \\
%    
    \begin{split}
         \tR[_i^j]
              &= - \tP[_i^j] - \frac{1}{2} \partial_t \tchi[_i^j] 
                 + \frac{1}{4} \left(
                     2 \tchi[_i_l] \tchi[^l^j]  
                     - \tchi[_i^j] \tchi[_l^l]
              \right)
               = 8\pi G \left( \tT[_i^j] - \frac{1}{2} 
                                               \tdelta[_i^j] T \right)
    \end{split} \..                                              \label{sync:syncEinstij}
    \end{align}
                                                                   \label{sync:syncEinst}
    \end{subequations}

    One other feature of the synchronous frame of reference is that gravitational fields 
    \emph{cannot} be stationary, because, if it is so,

    \begin{align*}
        \chi_{ij} = 0
        \,,
    \end{align*}

    \noindent
    causing a contradition to immerse in \eqref{sync:syncEinst00} the matter content
    carried by (or built-in) the energy-momentum tensor, so indeed the metric cannot be 
    stationary. Moreover, from \eqref{sync:syncEinstij}, in the empty space, we would have

    \begin{align*}
        P_{ij} = 0
        \,,
    \end{align*}

    \noindent
    corresponding to a null curvature and thus a flat $\mathds{R}^{n-1}$ Euclidian space.

    \

    Real particles also are not at rest in this frame of reference, because in general,
    the pressure exerted by the matter fields and the cosmological fluid have a spatial 
    component which will not move along the synchronous hypersheets.

}}}

\fancyend
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ ------------------ // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
