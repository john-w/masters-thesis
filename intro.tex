%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- INTRODUCTION -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
    \lettrine[lines=\lettrinelines]{\color{C_MainHighlight} \bf T}{he}
    construction of knowledge and our better understanding of the physical phenomena that
    manifests in Nature are one of the most valuable assets of the human kind. Even back in
    the day when the acquired knowledge was passed down to generations by the \emph{mythos}
    -- the word of mouth --, explanations of the vastly unknown Nature arose. Still it was
    not until the Greeks that deeper inquires about Nature and her inner workings emerge. 
    This ``uneasiness'' led to many questions such as: Why do bodies move? Why do they stay 
    put? What make them move? Why do things ``prefer'' other things? Where are we living 
    in? What are those lights in the night sky? This pursuit resulted in the very first
    rudimentary physical models, including models of the Universe, in which the Earth 
    resided in the center and everything else revolved around it.
    
    \

    Roughly two millennia later, Nicolaus Copernicus, upset with the strange aparent orbit
    of Mars and the increasing demand for the addition of epicycles in order to correct
    the orbits of ``errant stars'', or planets, and to explain their retrograde motions in
    the celestial dome, proposed a model of the Universe (namely, of our Solar system)
    where the Sun was at the center instead and Isaac Newton formulated the first 
    mathematical model for gravitation via an inverse square action at a distance law, where 
    the first assertion of the \emph{Cosmological principle} appeared. The enormous success 
    of Newtonian mechanics and gravitation consolidated most of the physics for the next 
    centuries, but as technology improved, more it seemed that the Newtonian physics lacked 
    something.

    \

    Such discomfort drove the stark reformation of our very way of thinking in the
    beginning of the 20th century. Challenging both the fundamental premises which preceded
    all the contemporary theories and our general understanding of them, the birth of 
    disciplines such as Quantum Mechanics and the physics of Relativity marked this new 
    era. Albert Einstein was one of the iconic figures whose concern about the 
    description of electromagnetic phenomena by families of inertial observers led him to 
    the reformulation of Galilean relativity, giving rise to the \emph{Special Theory of 
    Relativity}\cite{ART:1905AnP...322..891E} and later on, going a step further towards 
    gravitational effects, generalized his findings to what we call \emph{General Theory of 
    Relativity}\cite{ART:1916AnP...354..769E}.

   \

    To be more precise, Einstein was bothered by the discrepancies that would be found by
    different inertial observers in the outcome of a given eletromagnetic experimental 
    apparatus, one at rest with it and the other moving with a speed $v$ relative to it,
    when the traditional Galilean change of reference frame was performed to dinamical 
    physical quantities. In order to address that issue, he postulates two fundamental
    premises that were very reasonable and were already sustained by experimental data:

    \begin{enumerate}
        \item {\bf Constancy of $c$:} The speed of light $c$ has the same 
              numerical value in \emph{all} inertial frames of reference;
        \item {\bf Principle of relativity:} The laws of Physics must be the same on 
              all inertial frames of reference.
    \end{enumerate}

    It was then possible to construct a new set of coordinate transformations from 
    the ground up in such a way that any theory build with those two
    postulates in mind were automatically \emph{covariant}, that is, valid in every
    inertial frame of reference. However, that comes with a price: the notion of
    \emph{absolute simultaneity} is now broken, in such a way that it depends on which 
    frame you are in.  That prompted a construction of a new 4-dimensional space where now 
    the time is treated as a coordinate, so that the transformation of coordinates is now 
    done by the \emph{Lorentz transformations}. That space was dubbed \emph{spacetime} and 
    each point $P = (ct, x)$ in it represents an \emph{event}. Furthermore, this spacetime 
    structure naturally bears a \emph{causal} structure of events.

    \

    Now, since the notion of absolute frames of reference is completely lost, it is
    necessary to construct \emph{invariant} quantities that are independent of an
    arbitrary choice of frame of reference. One of such quantities is the so called
    \emph{interval} $\Delta s^2$, which is a quadratic form with a \emph{Lorentz
    signature} and is characterized by the quadratic differences of the space and
    time coordinates

    \begin{align*}
        \Delta s^2 = (c \Delta t)^2 - \Delta \boldsymbol{x}^2
                                                                                         \,.
    \end{align*}

    Nevertheless, that still was not enough; the second postulate above was too restrictive
    and incompatible while dealing with accelerated frames of reference, such as a falling
    frame on a gravitational field. To address that, Einstein revisited the principle of
    relativity and appended a few more reasonable others

    \begin{enumerate}
        \item {\bf Constancy of $c$:} The speed of light $c$ has the same 
              numerical value in \emph{all} frames of reference;
        \item {\bf Principle of \emph{General} Relativity:} The laws of Physics must be the
              same on all frames of reference;
        \item {\bf Principle of General Covariance:} The mathematical equations and their
              numerical constants must be invariant upon a change of reference frame;
        \item {\bf Equivalence Principle:} All bodies are equally accelerated in the
              presence of gravitational forces, following geodesic worldlines, regardless 
              of their nature;
        \item {\bf Mach's Principle:} The spacetime structure is influenced by matter.
    \end{enumerate}

    Just like what happened with the Special Theory of Relativity -- as we discussed 
    above --, these postulates drastically changed how we view the gravitational theory, as 
    we will discuss later. Now, spacetime itself was geometrized and gravitational forces 
    were just a consequence of it in the light of the fourth postulate, so there is a 
    natural need to introduce a \emph{Riemannian} metric space to properly describe 
    gravitational phenomena.

    \

    This new formulation of gravity trigged a whole new era for Cosmology where Newton's
    cosmological principle broadens up to accomodate the five postulates of General
    Relativity and reads as

    \begin{itemize}
        \item[ ] {\bf Cosmological Principle:} The laws of Physics must be the same
                 everywhere in the Universe.
    \end{itemize}

    Many were the proposed relativistic cosmological models, each and every one still
    challenged by the ever growing number of more precise data and sophisticated and modern
    observational techniques, two of such information indispensable for any model: 
    \emph{the Universe is expanding}, as pointed out by the receding motion of galaxies 
    discovered by Hubble and Humason%
    \cite{ART:1929PNAS...15..168H} and the discovery and measurement of the \emph{Cosmic
    Microwave Background}\cite{ART:1965ApJ...142..419P} (CMB), both making up the 
    strongest evidences supporting the \emph{big-bang}.

    \abreviatura{\CMB}{Cosmic Microwave Background}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=.5\textwidth]{res/MISC__CMB.png}
        \caption{Cosmic Microwave Background (From NASA\cite{ART:WMAP2013ApJS..208...20B}).}
        \label{fig:intro:CMB}
    \end{figure}

    \

    For instance, the \emph{Steady State} model proposed by Bondi and Gold%
    \cite{ART:1948MNRAS.108..252B} is a theory where the Universe is stationary but still
    expanding according to Hubble's law. The authors propose what they call the
    \emph{Perfect Cosmological Principle}, extending the validity of the usual 
    Cosmological Principle to \emph{all epochs}. However, their theory was fundamentally
    incompatible with the CMB discovery.

    \

    The most promising cosmological model to survive the trials of Nature is that of
    Friedmann, Roberton, Lema\^itre and Walker indepently found between the 1920s and 1930s, 
    together with the \emph{Inflationary scenario} proposed by Guth%
    \cite{ART:Guth1981PhRvD..23..347G,BK:Linde1990,MSC:Starobinskii1996tyli.book..767S} 
    to address several pertinent issues of it, which constitutes the standard model for the 
    Universe nowadays, though it still under scrutiny.  Another contesting model that is 
    still revisited is the Brans and Dicke \emph{Scalar Theory of Gravitation}% 
    \cite{ART:1961PhRv..124..925B}. In this one the authors attempt to fully integrate the
    Mach's Principle into the theory by the introduction of a scalar field in the place of 
    the inverse of Newton's universal gravitational constant, $\phi \propto G^{-1}$.

    \

    In parallel with that, relativistic Field Theory and Quantum Mechanics got traction 
    and, success after success, the Quantum Field Theory was developed, considered nowadays 
    the most fundamental theory where all others emerge at low-energy. Constructed upon 
    the mathematical framework of Symmetry Groups and the \emph{Gauge principle}, it was 
    found that three of the four fundamental interactions of Nature can be accomodated in 
    the so-called \emph{Standard Model}, except for gravity. This along with the apparent 
    convergence of the coupling constants of \emph{all} fields at higher energies%
    \cite{ART:Boer1994PrPNP..33..201D}, including gravity, points out to a \emph{Theory of
    Everything}. At this point, the quest for Quantum Gravity begins.

    \begin{figure}[h!]
        \centering
        \includegraphics[width=.5\textwidth]{res/MISC__CouplingConstants.png}
        \caption{The coupling constants of the fundamental interactions converge at higher
                 energies (Extracted from Boer\cite{ART:Boer1994PrPNP..33..201D}).}
        \label{fig:intro:couplingConstants}
    \end{figure}

    \

    Started by Weyl %\cite{<+citeWeyl+>} 
    in the late 1920s, the quantum gravity program 
    quickly became one of the main goals of modern physics but due to the non-linearity of 
    the Einstein equations and to mathematical inconsistences in the quantization program of
    field theories, at least from what we know thus far, of interacting particles of higher 
    spins, such as the spin 2 \emph{Graviton}, via the gauge principle!

    \

    One promising approach to solve this conundrum seems to be the description of
    gravitational phenomena in a weak-field approximation, where by working directly with
    Symmetry Groups and the associated spacetime symmetries, it is possible to explore
    Gravitation in a linear fashion, which, in our view, seems to be the most safe path
    towards quantization.

    \

    In the present work we adopt precisely this approach. By studying the construction of
    the Classical Theory of Gravitation via Symmetry Groups and by classifying all the
    possible algebras it comports and their main properties, we prepare the groundwork 
    with the indispensable tools to further pursue Quantum Gravity, resurrecting the most
    important, and still mostly forgotten works spread over in the literature on this 
    subject.

    \

    Although important, this study does not focus on solutions to Einstein equations to
    specific distributions of matter; our main interest is instead the \emph{geometric
    aspect} for models described by the three of the most relevant degrees of symmetry 
    in a descending order: from the maximal amount possible to the least crucial symmetries 
    any model ought to have.

    \

    In the first chapter we make a comprehensive recapitulation of the necessary tools, 
    where we demonstrate the principal results of Differential Geometry, introducing the
    notions of Killing vectors and local inertial frames of reference defined by the 
    N-Tuples.

    \

    Next, in chapter \ref{chap:maxsym}, we assume the spacetime to have the maximal number 
    of symmetries, using the formalism of Killing vectors and deducing its main properties. 
    By demonstrating the subdivision theorem, we show that the standard model of 
    Cosmology follows from a maximally symmetric 3-space, at the point where we discuss it 
    a bit and show some of the main results.

    \

    Moving forward to chapter \ref{chap:bianchi}, we consider only the homogeneity of the
    space sector of spacetime. There, in the local N-Tuple frame, we show that the
    homogeneous 3-spaces defines surfaces of transitivity associated with a Lie group, 
    reducing to one of the nine possible classes of three parameter Lie groups, which makes 
    up the Bianchi classification.

    \

    In chapter \ref{chap:petrov}, we consider the inherently symmetries of the curvature 
    tensor, which has to be present in any theory of gravitation. By studying the algebraic 
    properties of this tensor, we can bring it down to its principal axes related with the 
    geometrical invariants, making up the Petrov classification which contains three unique 
    classes plus three special degenerate ones.

    \

    Finaly, we present an overall discussion and close this monograph point out some future
    perspectives of investigations.

    \fancyend
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ ------------ // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
