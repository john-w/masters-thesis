%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- APPENDIX - BIVECTORS -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
    From the basic Linear Algebra, we learn the fundamental concept of vector spaces,
    which is the natural home of entities called vectors that satisfy a set of axioms
    pretty similar to those of Group Theory, which are, for three vectors $\bs{u}$,
    $\bs{v}$ and $\bs{w}$ of a vector space $V$, endowed with addiction and
    the multiplication by scalars, and for two scalars $a$ and $b$

    \begin{subequations}
    \begin{align}
        \begin{split}
            \bs{u} + \bs{v} &= \bs{v} + \bs{u}
        \end{split}
        &&\text{(commutativity)}
                                                             \label{biv:axiomAddCommutative}
                                                             \,, \\
        \begin{split}
            \bs{v} + (\bs{u} + \bs{w}) &= (\bs{v} + \bs{u}) + \bs{w}
        \end{split}
        &&\text{(associativity)}
                                                           \label{biv:axiomAddAssociativity}
                                                           \,, \\
        \begin{split}
            \bs{u} + \bs{0} &= \bs{u}
        \end{split}
        &&\text{(identity)}
                                                                \label{biv:axiomAddIdentity}
                                                                \,, \\
        \begin{split}
            \bs{u} + (\bs{-u}) &= \bs{0}
        \end{split}
        &&\text{(inverse)}
                                                                 \label{biv:axiomAddInverse}
                                                                 \,, \\
        \begin{split}
            a(\bs{v} + \bs{u}) &= a\bs{v} + a\bs{u}
        \end{split}
        &&\text{(scalar distributivity)}
                                                        \label{biv:axiomMulDistributiveVecs}
                                                        \,, \\
        \begin{split}
            (a + b)\bs{u} &= a\bs{u} + b\bs{u}
        \end{split}
        &&\text{(distributivity of scalars)}
                                                     \label{biv:axiomMulDistributiveScalars}
                                                     \,, \\
        \begin{split}
            (ab)\bs{u} &= a(b\bs{u})
        \end{split}
        &&\text{(scalar associativity)}
                                                           \label{biv:axiomMulAssociativity}
                                                           \,, \\
        \begin{split}
            1\bs{u} &= \bs{u}
        \end{split}
        &&\text{(scalar identity)}
                                                                \label{biv:axiomMulIdentity}
                                                                \..
    \end{align}
                                                                          \label{biv:axioms}
    \end{subequations}

    It is also seen two multiplicative operations \emph{between} vectors, the \emph{scalar
    product} (also called \emph{dot product} or \emph{inner product}), a bi-linear 
    operation that takes two vectors into a number ``$\cdot$''$: V \times V \rightarrow 
    \mathds{R}$, and the \emph{vector product} (or \emph{cross product}), an 
    anti-commutative bi-linear operation that takes two vectors into another ``$\times$''$: 
    V \times V \rightarrow V$. The former allow us to define the \emph{norm} (which is also 
    called the magnitude or length) of a vector by

    \begin{align}
        ||\bs{u}|| := \sqrt{\bs{u} \cdot \bs{u}}
                                                                         \label{biv:vecNorm}
    \end{align}

    \noindent
    and the latter defines a vector perpendicular to the plane defined by two vectors
    \emph{only in a $3-$dimensional space}

    \begin{align}
        \bs{w} = \bs{u} \times \bs{v}
        \,; \qquad
        \bs{w} \perp \bs{u} \,,
        \bs{w} \perp \bs{v}
                                                                         \label{biv:vecPerp}
                                                                         \,,
    \end{align}

    \noindent
    where its norm

    \begin{align}
        ||\bs{u} \times \bs{v}|| = ||u||\  ||v|| \sin\theta
                                                                     \label{biv:vecPerpNorm}
    \end{align}

    \noindent
    defines the area of the parallelogram delimited by both vectors and $\theta$ is 
    the angle between them. Also, the anti-symmetry defines an orientation for the 
    vector space.

    \

    Nevertheless, the vector product \emph{does not} enjoy certain desirable qualities.
    For instance, it cannot be defined in two dimensions (the orthogonal would
    pop out into a higher dimentions) and perpendicular vectors are only uniquely defined
    in three dimensions. The notion of a \emph{bi-vector} emerges to address these issues.

    \

    A \emph{bi-vector} is defined by means of a new product of 
    vectors, the \emph{outer product} (or \emph{exterior product}) which is defined as a
    bi-linear operation that takes two vectors into a bi-vector%
    \footnote{The ``$\wedge$'' (wedge) symbol is used to denote this multiplication and
    $Bi$ is the vector space containing all the bi-vectors.}
    ``$\wedge$''$: V \times V \rightarrow Bi$ and it has the following properties, for
    $\bs{u}$, $\bs{v}$ and $\bs{w} \in V$,

    \simbolo{$\wedge$}{Outer product}

    \begin{subequations}
    \begin{align}
        \begin{split}
            \bs{u} \wedge \bs{v} &= - \bs{v} \wedge \bs{u}
        \end{split}
        &&\text{(anti-symmetry)}
                                                                    \label{biv:outerAntisym}
                                                                    \\
        \begin{split}
            \bs{u} \wedge(\bs{v} + \bs{w}) &=
            \bs{u} \wedge \bs{v} + \bs{u} \wedge \bs{w}
        \end{split}
        &&\text{(distributivity)}
                                                               \label{biv:outerDistributive}
                                                               \..
    \end{align}
                                                                    \label{biv:outerProduct}
    \end{subequations}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=.3\textwidth]{res/Biv__primordialPlane.pdf}
        \captionof{figure}{Geometrical representation of the outer product and its
        orientation.}
        \label{fig:bivPrimordialPlane}
    \end{figure}

    Those objects possess a natural way to encode geometrical information.
    More precisely, we can associate each bi-vector to the \emph{oriented} plane defined by 
    $\bs{u}$ and $\bs{v}$, where the orientation can be seen by sweeping one vector onto
    the other. The bi-vector space can be interpreted as the \emph{set of all planes in the
    actual space}.

    \

    Now, if we take both $\bs{u}$ and $\bs{v}$ and decompose them into a $N-$dimensional 
    basis $\{\bs{e}_i\}$, for $i = 1, \ldots, N$, then

    \begin{align}
        \bs{u} \wedge \bs{v} 
        &= u^i v^j \bs{e}_i \wedge \bs{e}_j
        = U^{(ij)}\bs{E}_{(ij)}
                                                           \label{biv:bivectorDecomposition}
                                                           \,,
    \end{align}

    \noindent
    where the product $U^{(ij)} := u^i v^j$ is necessarily anti-symmetric and

    \begin{align}
        \bs{E}_{(ij)} &:= \bs{e}_i \wedge \bs{e}_j
                                                                   \label{biv:bivectorBasis}
    \end{align}

    \noindent
    form the $\frac{1}{2}N(N-1)$-dimensional basis of the bi-vector space on the
    anti-symmetric pairs $(ij)$, consisting of primitive planes defined by the basis
    vectors $\{\bs{e}_i\}$. We introduce a new family of indices $A$ that runs through all
    the possible $\frac{1}{2}N(N-1)$ combinations of the pairs $(ij)$


    \begin{align*}
        A := 
        \{&(01), (02), \ldots, (0N),
                                                                                \nonumber \\
        &(12), (13), \ldots, (1N),
                                                                                \nonumber \\
        &\vdots
                                                                                \nonumber \\
        &(N1), (N2), \ldots, (N\ N-1)\}
        \,,
    \end{align*}

    \noindent
    such that

    \begin{align}
        \bs{u} \wedge \bs{v} 
        &= U^A E_A
                                                 \label{biv:bivectorDecompositionBivIndices}
                                                 \..
    \end{align}

    To consolidate all these abstract concepts, we finalize by considering a few examples
    in two, three and four dimensions.


\subsection*{Example: Two dimensions}
{{{
    In this case we have only two basis vectors $\{\bs{e}_1,\bs{e}_2\}$:

    \begin{align*}
        \bs{u} \wedge \bs{v}
        &= 
        u^i v^j \bs{e}_i \wedge \bs{e}_j
\iftoggle{steps}
{
                                                                                \nonumber \\
        &=
        u^1 v^1 \underbrace{\bs{e}_1 \wedge \bs{e}_1}_{=0} + 
        u^1 v^2 \bs{e}_1 \wedge \bs{e}_2 + 
        u^2 v^1 \bs{e}_2 \wedge \bs{e}_1 + 
        u^2 v^2 \underbrace{\bs{e}_2 \wedge \bs{e}_2}_{=0}
}{}
                                                                                \nonumber \\
        &=
        (u^1 v^2 - u^2 v^1) \bs{e}_1 \wedge \bs{e}_2
        \equiv
        U^1 \bs{E}_1
    \end{align*}

    \noindent
    with

    \begin{align*}
        U^1 &\equiv (u^1 v^2 - u^2 v^1)
                                                                                \nonumber \\
        \bs{E}_1 &\equiv \bs{e}_1 \wedge \bs{e}_2
        \..
    \end{align*}

    We see that $\frac{1}{2}N(N-1)$ for $N=2$ really gives only one bi-vector, as expected.
}}}

\subsection*{Example: Three dimensions}
{{{ 
    Next, for $N=3$, we have three basis vectors $\{\bs{e}_1,\bs{e}_2,\bs{e}_3\}$.

    \begin{align*}
        \bs{u} \wedge \bs{v}
        &= 
        u^i v^j \bs{e}_i \wedge \bs{e}_j
\iftoggle{steps}
{  
                                                                                \nonumber \\
        &=
        u^1 v^2 \bs{e}_1 \wedge \bs{e}_2 + 
        u^1 v^3 \bs{e}_1 \wedge \bs{e}_3 + 
        u^2 v^1 \bs{e}_2 \wedge \bs{e}_1 + 
        u^2 v^3 \bs{e}_2 \wedge \bs{e}_3 + 
        u^3 v^1 \bs{e}_3 \wedge \bs{e}_1 + 
        u^3 v^2 \bs{e}_3 \wedge \bs{e}_2
}{}
                                                                                \nonumber \\
        &=
        (u^1 v^2 - u^2 v^1) \bs{e}_1 \wedge \bs{e}_2 
                                                                                \nonumber \\
                                                                                &\qquad
        +
        (u^2 v^3 - u^3 v^2) \bs{e}_2 \wedge \bs{e}_3 
                                                                                \nonumber \\
                                                                                &\qquad
        +
        (u^3 v^1 - u^1 v^3) \bs{e}_3 \wedge \bs{e}_1
                                                                                \nonumber \\
        &\equiv
        U^A \bs{E}_A
        \,,
    \end{align*}

    \noindent
    where $A = 1, 2, 3$ and

    \begin{align*}
        U^A \equiv \begin{cases}
            (u^1 v^2 - u^2 v^1)\,, \quad A = 1 = (12) \\
            (u^2 v^3 - u^3 v^2)\,, \quad A = 2 = (23) \\
            (u^3 v^1 - u^1 v^3)\,, \quad A = 3 = (31)
        \end{cases}
        \\
        \bs{E}_A \equiv \begin{cases}
            \bs{e}_1 \wedge \bs{e}_2\,, \quad A = 1 = (12) \\
            \bs{e}_2 \wedge \bs{e}_3\,, \quad A = 2 = (23) \\
            \bs{e}_3 \wedge \bs{e}_1\,, \quad A = 3 = (31)
        \end{cases}
        \..
    \end{align*}
}}}

\subsection*{Example: Four dimensions}
{{{
    Since we have not imposed anything about metric signatures in this formalism, we will
    consider a basis containing a ${}^0$ component
    $\{\bs{e}_0,\bs{e}_1,\bs{e}_2,\bs{e}_3\}$. We make this choice instead of the usual
    all-spatial vector spaces to illustrate the quantities we dealt with in 
    Chapter \ref{chap:petrov}, but rest assured that this formalism is valid \emph{for all}
    kinds of vector spaces, which includes the Minkowski case $\mathds{M}^4$ of the
    aforementioned chapter. Thus, we have six primitive basis planes and

    \begin{align*}
        \bs{u} \wedge \bs{v}
        &= 
        u^i v^j \bs{e}_i \wedge \bs{e}_j
\iftoggle{steps}
{  
                                                                                \nonumber \\
        &=
        u^0 v^1 \bs{e}_0 \wedge \bs{e}_1 + 
        u^0 v^2 \bs{e}_0 \wedge \bs{e}_2 + 
        u^0 v^3 \bs{e}_0 \wedge \bs{e}_3 + 
        u^1 v^0 \bs{e}_1 \wedge \bs{e}_0 + 
        u^1 v^2 \bs{e}_1 \wedge \bs{e}_2 + 
        u^1 v^3 \bs{e}_1 \wedge \bs{e}_3 + 
                                                                                \nonumber \\
        &\qquad\qquad
        u^2 v^0 \bs{e}_2 \wedge \bs{e}_0 + 
        u^2 v^1 \bs{e}_2 \wedge \bs{e}_1 + 
        u^2 v^3 \bs{e}_2 \wedge \bs{e}_3 + 
        u^3 v^0 \bs{e}_3 \wedge \bs{e}_0 + 
        u^3 v^1 \bs{e}_3 \wedge \bs{e}_1 + 
        u^3 v^2 \bs{e}_3 \wedge \bs{e}_2
}{}
                                                                                \nonumber \\
        &=
        (u^0 v^1 - u^1 v^0) \bs{e}_0 \wedge \bs{e}_1 
                                                                                \nonumber \\
                                                                                &\qquad
        +
        (u^0 v^2 - u^2 v^0) \bs{e}_0 \wedge \bs{e}_2 
                                                                                \nonumber \\
                                                                                &\qquad
        +
        (u^0 v^3 - u^3 v^0) \bs{e}_0 \wedge \bs{e}_3 
                                                                                \nonumber \\
                                                                                &\qquad
        +
        (u^1 v^2 - u^2 v^1) \bs{e}_1 \wedge \bs{e}_2 
                                                                                \nonumber \\
                                                                                &\qquad
        +
        (u^2 v^3 - u^2 v^3) \bs{e}_2 \wedge \bs{e}_3 
                                                                                \nonumber \\
                                                                                &\qquad
        +
        (u^3 v^1 - u^3 v^1) \bs{e}_3 \wedge \bs{e}_1
                                                                                \nonumber \\
        &\equiv
        U^A \bs{E}_A
        \,,
    \end{align*}

    \noindent
    where evidently

    \begin{align*}
        U^A \equiv \begin{cases}
            (u^0 v^1 - u^1 v^0) \,, \quad A = 0 = (01) \\
            (u^0 v^2 - u^2 v^0) \,, \quad A = 1 = (02) \\
            (u^0 v^3 - u^3 v^0) \,, \quad A = 2 = (03) \\
            (u^1 v^2 - u^2 v^1) \,, \quad A = 3 = (12) \\
            (u^2 v^3 - u^2 v^3) \,, \quad A = 4 = (23) \\
            (u^3 v^1 - u^3 v^1) \,, \quad A = 5 = (31)
        \end{cases}
        \,, \\
        \bs{E}_A \equiv \begin{cases}
            \bs{e}_0 \wedge \bs{e}_1\,, \quad A = 0 = (01) \\
            \bs{e}_0 \wedge \bs{e}_2\,, \quad A = 1 = (02) \\
            \bs{e}_0 \wedge \bs{e}_3\,, \quad A = 2 = (03) \\
            \bs{e}_1 \wedge \bs{e}_2\,, \quad A = 3 = (12) \\
            \bs{e}_2 \wedge \bs{e}_3\,, \quad A = 4 = (23) \\
            \bs{e}_3 \wedge \bs{e}_1\,, \quad A = 5 = (31)
        \end{cases}
        \..
    \end{align*}


}}}

    The formulation of the outer product generalization to arbitrary dimensions,
    as seen in the examples above, alongside with the already established inner
    product, enabled Grassmann to devise a whole new branch of algebra, the 
    \emph{Geometric Algebra}%
    \cite{BK:Grassmann1862,BK:Lasenby2003,BK:Lounesto2001,BK:Hestenes1984},
    mostly centered around yet another kind of product, the \emph{geometric product}, 
    that is nothing but adding together both inner and outer products.

    \

    While not taking flight at first, Grassmann's pioneering works were the rudiments for
    the theory of exterior algebra and, later on, Clifford Algebra, no less, the
    latter being nowadays the backbone of many fundamental theories, such as the Quantum
    Field Theory.

    \fancyend

}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ -------------------- // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
