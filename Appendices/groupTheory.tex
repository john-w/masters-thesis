%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- GROUP THEORY -- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
{
    The concept of \emph{groups} in physics can be roughly thought as a set of operations
    that leave something or some quality invariant. With that in mind we can readily see
    how that is a fundamental ingredient to incorporate in most, if not all, physical
    theories constructed \emph{ab initio}. For instance, the principle of covariance may be
    understood in that light, with the set of operations being a general transformation of
    coordinates that leave the equations of motion invariant, that is, the geodesics
    themselves, which are closely related with the associated field equations and particle
    dynamics.

    \

    Group theory is a vast topic and treatises can be written about it, so we will just
    focus on the concepts necessary to comprehend the main subject of this monograph.

\section{Abstract Group Theory}
\label{ssec:absGroups}
{{{
    A set $G$ of elements $g$ endowed with a bilinear product operation $G \cdot G
    \rightarrow G$ is defined as a \emph{group} if all elements $\{g_i\} \in G$ (not
    necessarely discrete) satisfy:

    \simbolo{$G$}{Group}

    \begin{subequations}
    \begin{align}
        \begin{split}
        & g_1 \cdot g_2 \in G
                                                           \label{grp:groupAxiomsClosedness}
                                                           \,,
        \end{split}
        &\text{(closure)} \\
        \begin{split}
        & g_1 \cdot (g_2 \cdot g_3) = (g_1 \cdot g_2) \cdot g_3
                                                        \label{grp:groupAxiomsAssociativity}
                                                        \,,
        \end{split}
        &\text{(associativity)} \\
        \begin{split}
        & \exists\ e \in G\ 
                   \,; \quad g \cdot e = e \cdot g = g
                                                             \label{grp:groupAxiomsIdentity}
                                                             \,,
        \end{split}
        &\text{(identity)} \\
        \begin{split}
        & \exists\ g^{-1} \in G\ 
                 \,; \quad g \cdot g^{-1} = g^{-1} \cdot g = e
                                                              \label{grp:groupAxiomsInverse}
                                                              \..
        \end{split}
        &\text{(inverse)}
    \end{align}
                                                                     \label{grp:groupAxioms}
    \end{subequations}

    \simbolo{$e$}{Group identity}
    \simbolo{$g^{-1}$}{Group inverse element}
    \
    
    A subset of elements $h \in G$ which satisfy all the group axioms 
    \eqref{grp:groupAxioms} by themselves and inherits the same multiplication operation is 
    called a \emph{subgroup} of $G$, where evidently $H \subseteq G$. For example, 
    $\gSO(n;\mathds{R})$ is a subgroup of $\gO(n;\mathds{R})$, which in turn is a subgroup 
    of $\gGL(n;\mathds{R})$ 
    ($\gO(n;\mathds{R}) \subseteq \gSO(n;\mathds{R}) \subseteq \gGL(n;\mathds{R})$). Every
    group defines its own subgroups.

    \

    A map from a group $G$ into another $G'$ preserving the multiplication operation
    defines a \emph{homomorphism} $\Phi: G \rightarrow G'$. If the mapping is one-to-one 
    (both injective and surjective, admiting inverse), then it gets called an 
    \emph{isomorphism} ($G' \approx G$). If $G' = G$, the mapping is called an 
    \emph{endomorphism}. If it is one-to-one, then it is called an \emph{automorphism}. 
    The set of elements $g \in G$ which maps onto $e' \in G'$ is called 
    the \emph{kernel} of the homomorphism.

    \simbolo{$\approx$}{Isomorphism}

    \

    The \emph{center} $Z$ of a group $G$ is the set of all elements $z \in G$ that commutes
    with all the other group elements, i.e., $zg = gz$ $\forall g \in G$. If all the  
    elements of $G$ commute between themselves, the group is called \emph{Abelian}. Thus,
    by \eqref{grp:groupAxiomsIdentity}, the identity $e$ is always in the center of any 
    group.

    \

    For any given element $s_0$ of any set $S$, we define the \emph{orbit} of $s_0$ with 
    respect to $G$ as the subset of all elements that can be obtained from $s_0$ by the 
    action of $G$, denoted by $O_G(s_0)$, where the action is the multiplication operation
    of the group in $S$.

    \simbolo{$O_G(s_0)$}{Orbit of $s_0$ with respect to $G$}

    \

    One example to illustrate the last two concepts naturally emerge from the particular
    group of rigid rotations%
    \footnote{ $\gSO(n)$ is to be undestood as $\gSO(n; \mathds{R})$ henceforward, 
               unless explicitly stated.}
    $\gSO(n)$ acting on $\mathds{R}^n$: the center of the group is the identity element $e$ 
    whereas the orbit are the surfaces of the spheres of radius $r$, $0 \le r < \infty$ in 
    Euclidian spaces.

    \

    The \emph{surface of transitivity} is defined such that
    any element $s \in O_G(s_0)$ can be obtained from any other group transformation
    of another element of the orbit, that is, $s_1, s_2 \in O_G(s_0) \,;$ $g s_1 = s_2\,,$ 
    $\forall g \in G$.

    \

    The transformation of the element $g$ given by $g \rightarrow h g h^{-1}$ for 
    $g,h \in G$ is called a \emph{conjugation}. Similarly, a \emph{group conjugation}
    occurs when such transformation is applied to all elements of $G$, that is, $g
    \rightarrow h g h^{-1}\,,\ \forall g \in G$ (symbolically $G \rightarrow h G h^{-1}$
    for some $h \in G$). This preserves the group multiplication

    \begin{align*}
        gg' &\rightarrow h (gg') h^{-1} 
                                                                                \nonumber \\
        &\rightarrow h (g e g') h^{-1} 
                                                                                \nonumber \\
        &\rightarrow h g (h^{-1} h) g'h^{-1} 
                                                                                \nonumber \\
        &\rightarrow (h g h^{-1}) (h g'h^{-1}) 
        \,,
    \end{align*}

    \noindent
    for some $g,g',h \in G$.

    \

    The conjugation of a fixed $h \in G$ by the entirety of $G$, $g h g^{-1}\,,\ \forall g
    \in G$, defines the \emph{conjugation class} of $h$, which clearly is an orbit of $h$
    $O_G(h)$.

    \

    The conjugation permits us to finally define the \emph{normal subgroup} (or 
    \emph{invariant subgroup}) $H$ of $G$, whose elements are invariant under conjugation, 
    i.e., $h \subseteq g h g^{-1}$, $\forall h \in H$ and $\forall g \in G$, symbolically
    denoted by $H \subseteq g H g^{-1}$. Now if the conjugation itself is in $H$, that is, 
    $g H g^{-1} \subseteq H$ for all $g \in G$, then the the equality is in place 
    $h = g h g^{-1}$ for some $h \in H$; this implies that $h$ is in the center 
    $h \in Z(G)$.

    \

    Yet still, we define the \emph{left} (right) \emph{coset} as the subgroup $H \subset G$
    with respect to the left (right) multiplication if $g_1$ and $g_2$ are in the
    same left (right) coset and if there exists $h \in H$ such that $g_1 = h g_2$ 
    ($= g_2 h$). The cosets of a groups are, by definition, orbits of $H$.

    \

    Now if $H$ is a normal subgroup of $G$, the set of all cosets of $G$ with respect
    to $H$ makes up a group. This group, denoted by $Q := G/H$, is called the
    \emph{quotient group} of $G$ and has $H$ as its identity element and its multiplation
    law is said to be \emph{induced} by $G$. In general the quotient group \emph{is neither} 
    a subgroup of $G$ nor isomorphic to it.

    \simbolo{$G/H$}{Quotient group}
}}}


\section{Lie Group Theory}
\label{ssec:LieGroups}
{{{
    Lie groups are those which contains a continuous ``amount'' of elements and are locally 
    Euclidian. This enables us to parametrize any element by a
    finite number of continuous variables, $g = g(a_1, \ldots, a_r)$, where $r$ is called
    the \emph{order} of the group, corresponding to the minimum number of parameters 
    necessary to fully describe the group; $\{a_k\}, k = 1, \ldots, r$ are continuous and, 
    by convention, $e \equiv g(0, \ldots, 0)$. Every Lie group is isomorphic to a matrix 
    group near the identity, to what Hall\cite{BK:Hall2004} calls the 
    \emph{Matrix Lie Group}.

    \

    As a consequence of the continuous parametrization, the Lie groups also defines a
    differentiable manifold $G$ (or $\mathcal{M}$ as used in the text) such that 
    the bilinear group product $G \cdot G \rightarrow G$, the elements and their inverses 
    are all differentiable. Even stronger than that, the following theorem guarantees that.

    \begin{theorem}
        Every matrix group is a smooth embedded submanifold of $\mathcal{M}(N)$ and is
        thus a Lie group.
    \end{theorem}

    \ 

    The smoothness of Lie groups is also preserved by the various morphisms between Lie
    groups.

    \

    Some of the most common Lie groups in a matrix representation, with ordinary matrix
    multiplication as the composition law, are given below.

    \

    \begin{enumerate}[i.]
        \item[]
        For a given \emph{field}%
        \footnote{Field is just the set of elements where the usual operations are defined, 
                  that is $\{+, -, \times, \div\}$, satisfying the field axioms. For 
                  example $\mathds{F} = \mathds{R}$ or $\mathds{C}$ (real or complex 
                  numbers).}
        $\mathds{F}$

        \item The \emph{General Linear Group} $\gGL(n; \mathds{F})$ of all invertible $n
              \times n$ matrices over $\mathds{F}$.

              This corresponds to the group of matrices and their usual multiplication.

        \item The \emph{Special Linear Group} $\gSL(n; \mathds{F})$ of all the elements $M
              \in \gGL(n; \mathds{F})$ with $\det M = +1$.

        \item The \emph{Orthogonal Group} $\gO(n; \mathds{F})$ of all $n \times n$ matrices
              $M$ over $\mathds{F}$ with $M^T M = \mathds{1}$.

        \item The \emph{Special Orthogonal Group} $\gSO(n; \mathds{F})$ of all the elements
              $M \in \gO(n; \mathds{F})$ with $\det M = +1$,

              This corresponds to the group of rotations over $\mathds{F}$.

        \item The \emph{Unitary Group} $\gU(n)$ of all the $n \times n$ complex
              ($\mathds{F}$ is necessarily $\mathds{C}$) matrices $M$ where 
              $M^\dagger M = \mathds{1}$.

        \item The \emph{Special Unitary Group} $\gSU(n)$ of all the elements $M \in \gU(n)$
              with $\det M = +1$.

        \item The \emph{Symplectic Group} $\gSp(n; \mathds{F})$ of all $2n \times 2n$
              matrices $M$ over $\mathds{F}$ with $M^T J M = J$, where 
              $J := \left(
              \begin{matrix} 
                  0 & \mathds{1}_n \\ 
                 -\mathds{1}_n & 0
              \end{matrix}
              \right)$. The
              condition imposed already implies $\det M = +1$.

              While it apparently looks like an uncommon group, it naturally arises in the 
              hamiltonian formalism of Classical Mechanics.

        \item The \emph{Lorentz Group}%
              \footnote{$\gO(n,k)$ is also called the \emph{Generalized Orthogonal Group}
                        with a metric signature defined by the inner product
                        $[x,y]_{n,k} = x_1y_1+\cdots + x_n y_n 
                        - x_{n+1} y_{n+1}-\cdots - x_{n+k}y_{n+k}$.
                       }
             $\gO(3,1)$ of all real $4 \times 4$ matrices 
             $\Lambda$ with $\Lambda^T \hat{\eta} \Lambda = \hat{\eta}$, i.e., 
             the group of general transformation of coordinates that leave the metric 
             tensor $\hat{\eta}$ (and thus the line element) invariant.
             
    \end{enumerate}

    A Lie group $G$ is said to be \emph{connected} if we can transform one element $g_1 \in
    G$ into another $g_2 \in G$ through a continuous parametrized path $g(t), t_1 \le t \le
    t_2$ where $g(t_1) = g_1$ and $g(t_2) = g_2$. Furthermore, we say a Lie group is 
    \emph{simply connected} if it is connected and every closed loop in $G$ can be shrunk 
    to a point in $G$. 

    \
   
    The simply connectedness condition is fundamental to get a one-to-one correspondence 
    between $G$ and of its Lie algebra.  In fact, the Lie algebra is 
    precisely the tangent space in the neighbourhood of the identity, thus locally 
    Euclidian. The connection between Lie groups and algebras will soon become clear when we 
    define the exponential map. But first let us properly define what is a Lie algebra.

    \

    If $V^n$ is a $n-$dimensional linear vector space endowed with the closed bilinear 
    multiplication $[V^n,V^n] \rightarrow V^n$, we say that $V^n$ is a \emph{Lie algebra} 
    $\frak{g}$ if for $X, Y, Z \in V^n$ 

    \begin{subequations}
    \begin{align}
        \begin{split}
            [X, Y] = -[Y, X]
                                                          \label{grp:lieAlgebraSkewSymmetry}
                                                          \,,
        \end{split}
        &\text{(Skew-symmetry)} \\
        \begin{split}
            [X, [Y, Z]] + [Y, [Z, X]] + [Z, [X, Y]] = 0
                                                        \label{grp:lieAlgebraJacobiIdentity}
                                                        \..
        \end{split}
        &\text{(Jacobi Identity)}
    \end{align}
                                                                \label{grp:lieAlgebraAxioms}
    \end{subequations}

    We call this product a \emph{commutator} or the \emph{Lie bracket} of $X$ and $Y$.
    Usually Lie algebras are denoted by lowercase Fraktur letters. If $[X, Y] = 0$, 
    $\forall X, Y \in \frak{g}$, we say that $\frak{g}$ is Abelian, similar to what we have
    seen before.

    \simbolo{$\frak{g}$}{Lie algebra}

    \

    If we take a complete basis $\{X_i\}\,,\ i = 1,\ldots, n$ for $\frak{g}$ we define the
    \emph{structure constants} $c^k_{ij}$ as

    \begin{align}
        [X_i, X_j] = c^k_{ij} X_k
                                                    \label{grp:lieAlgebraStructureConstants}
                                                    \,,
    \end{align}

    \noindent
    which are anti-symmetric in their lower indices $c^k_{ij} = - c^k_{ji}$ and 

    \begin{align}
        c^m_{il} c^l_{jk} + c^m_{jl} c^l_{ki} + c^m_{kl} c^l_{ij} = 0
                                      \label{grp:lieAlgebraJacobiIdentityStructureConstants}
                                      \,,
    \end{align}

    \noindent
    by virtue of \eqref{grp:lieAlgebraAxioms}. Upon a change of basis,

    \begin{align*}
        X'_a = d^i_a X_i
        \,, \quad d^i_j \neq 0
    \end{align*}

    \noindent
    we see that the structure constants transform like a (1,2) tensor

    \begin{align*}
        c'^k_{ij} d^a_k &= c^a_{bc} d^b_i d^c_j
                                                                                \nonumber \\
        \implies
        c'^k_{ij} &= c^a_{bc} d^k_a d^b_i d^c_j
        \..
    \end{align*}

    The \emph{exponential map} of a one-parameter subgroup $G$ of a Lie algebra $\frak{g}$
    associates a continuous element of that subgroup $A(t) \in G$ to its Lie algebra 
    \emph{generator} $X \in \frak{g}$ via

    \begin{align}
        A(t) = e^{tX} = \sum\limits_n \frac{(tX)^n}{n!}
                                                        \label{grp:lieAlgebraExponentialMap}
                                                        \,,
    \end{align}

    \noindent
    satisfying% 
    \footnote{To not confuse the reader, we shall change the notation of the identity
              element $e$ by $I$.}
              for $X, Y \in \frak{g}$, $A(t), B(t) \in G$

    \begin{subequations}
    \begin{align}
        \begin{split}
            A(0) &= e^0 = I
                                                                        \label{grp:expMapId}
        \end{split}
        \\
        \begin{split}
            A(t+s) &= A(t) A(s) \,, \quad \forall t,s \in \mathds{R}
                                                               \label{grp:expMapBiLinearity}
        \end{split}
        \\
        \begin{split}
            A^*(t) &= (e^{tX})^* = e^{tX^*}
                                                                   \label{grp:expMapAdjoint}
        \end{split}
        \\
        \begin{split}
            A^{-1}(t) &= (e^{tX})^{-1} = e^{-tX}
                                                                   \label{grp:expMapInverse}
        \end{split}
        \\
        \begin{split}
            A(t) B(t) &= e^{tX} e^{tY} = e^{tX+tY} \,, \quad \text{if $[X,Y]=0$}
                                                                   \label{grp:expMapClosure}
        \end{split}
        \\
        \begin{split}
            ||A(t)|| &= || e^{tX} || = e^{t||X||}
                                                                      \label{grp:expMapNorm}
        \end{split}
        \\
        \begin{split}
            e^{tCXC^{-1}} &= C e^{tX} C^{-1} \,, \quad C\ \text{invertible}
                                                                 \label{grp:expMapConjugate}
                                                                 \,,
        \end{split}
    \end{align}
                                              \label{grp:lieAlgebraExponentialMapProperties}
    \end{subequations}

    \noindent
    where $||\cdot||$ is the norm of a $n \times n$ matrix $M$ defined as

    \begin{align*}
        ||M|| = \left( 
            \sum\limits_{i,j=1}^n |M_{ij}|^2
        \right)^{\frac{1}{2}}
        \..
    \end{align*}

    In the case of noncommuting $X$ and $Y$, with $[X, [X,Y]] = 0$, 
    \eqref{grp:expMapClosure} becomes the famous Baker-Campbell-Hausdorff formula

    \begin{align}
        A(t) B(t) &= e^{tX} e^{tY} = e^{tX+tY+\frac{t}{2}[X,Y]}
                                                                       \label{grp:expMapBCH}
                                                                       \..
    \end{align}

    Since the Lie algebra defines a smooth submanifold, then by virtue of being analytical
    everywhere, the exponential map will also define the same smooth submanifold, thus  
    being differentiable. After that, we can finally link Lie groups to Lie algebras by

    \begin{align}
        \frac{d}{dt}e^{tX} \bigg|_{t = 0} = X
                                                             \label{grp:LieAlgebraFromGroup}
                                                             \..
    \end{align}

    This strong link between Lie groups and algebras guarantees that most of the properties
    of Lie groups seen in Sec. \ref{ssec:absGroups} have an algebraic analog, as we will
    see next. But first let us also list the Lie algebras associated with the most common 
    groups 

    \

    \begin{enumerate}[i.]
        \item[]
        For a given \emph{field} $\mathds{F}$

        \item The \emph{General Linear Lie Algebra} $\frak{gl}(n; \mathds{F})$ of all
              invertible $n \times n$ matrices over $\mathds{F}$ with $[X, Y] = XY - YX$ 
              for $X,Y \in \frak{gl}$.

        \item The \emph{Special Linear Lie Algebra} $\frak{sl}(n; \mathds{F})$ of all
              $n \times n$ matrices $X$ over $\mathds{F}$ with $\Tr X = 0$.

        \item The \emph{Orthogonal Lie Algebra} $\frak{o}(n; \mathds{F})$ 
              of all $n \times n$ matrices $X$ over $\mathds{F}$, which are
              anti-symmetric ($X^T = X^{-1}$)

        \item The \emph{Special Orthogonal Lie Algebra} $\frak{so}(n; \mathds{F})$
              of all $n \times n$ matrices $X$ over $\mathds{F}$, which are
              anti-symmetric ($X^T = X^{-1}$) and have $\Tr X = 0$.

        \item The \emph{Unitary Lie Algebra} $\frak{u}(n)$ of all the $n \times n$ complex
              matrices $X$ with $X^\dagger = X^{-1}$.

        \item The \emph{Special Unitary Lie Algebra} $\frak{su}(n)$ of all the 
              elements $X \in \frak{su}(n)$ with $\Tr X = 0$.

        \item The \emph{Symplectic Lie Algebra} $\frak{sp}(n; \mathds{F})$ of all 
              $2n \times 2n$ matrices $X$ over $\mathds{F}$ 
              with $X^T J = - J X$, where 
              $J := \left(
              \begin{matrix} 
                  0 & \mathds{1}_n \\ 
                 -\mathds{1}_n & 0
              \end{matrix}
              \right)$. 

              The elements of this algebra are of the form
              
              \begin{align*}
                  \left(
                  \begin{matrix}
                      A & B \\
                      C & -A^T
                  \end{matrix}
                  \right)
              \end{align*}

              \noindent
              with $A$ being an arbitrary $n \times n$ matrix and $B$ and $C$ arbitrary
              symmetric matrices.

        \item The \emph{Lorentz Lie Algebra} $\frak{o}(3,1)$ (or $\frak{l}_{3,1}$) 
              of all real $4 \times 4$ matrices $X$ with the Lorentz inner product
              and $\hat{\eta} X^T \hat{\eta} = - X$.

    \end{enumerate}

    A \emph{Lie subalgebra} $\frak{q} \subseteq g$ is a subset of elements in $\frak{g}$ 
    that is closed under the same multiplication operation $[\ ,\ ]$, that is, for $Q_1,
    Q_2 \in \frak{q}$, then $[Q_1, Q_2] \in \frak{q}$. For example 
    $\frak{su}(n) \subseteq \frak{u}(n)$.

    \

    We say $\frak{q} \subseteq \frak{g}$ is an \emph{ideal} of $\frak{g}$ if, for all 
    $X \in \frak{g}$ and for all $Y \in \frak{q}$, $[X, Y] \subset \frak{q}$ (symbolically 
    $[\frak{g},\frak{q}] \subset \frak{q}$). Ideals are also called \emph{invariant
    subalgebras} and are usually denoted by $\frak{q} \triangleleft \frak{g}$.

    \simbolo{$\triangleleft$}{Ideal}

    \

    Algebraic \emph{homomorphisms} are linear maps between algebras $\varphi : \frak{g}
    \rightarrow \frak{h}$ such that the Lie brackets are preserved, so that, for 
    $X, Y \in \frak{g}$,

    \begin{align}
        \varphi([X,Y]) = [ \varphi(X), \varphi(Y)]
                                                                    \label{grp:homomorphism}
                                                                    \..
    \end{align}

    If the mapping is one-to-one, then it is called an \emph{isomorphism}. On the other 
    hand, if the mapping takes $\frak{g}$ back into itself $\varphi: \frak{g} \rightarrow
    \frak{g}$, we say it is an \emph{endomorphism}.  The set of elements that map into 0,
    $\varphi^{-1}(0)$, is called the \emph{kernel} of the homomorphism, denoted by
    $\ker\varphi$.

    \simbolo{$\ker\varphi$}{Kernel of $\varphi$}

    \

    If an endomorphism is one-to-one, we call it an \emph{automorphism} and it represents 
    the \emph{group of motions} of the space. In this context, the generators are called 
    the \emph{actions} or \emph{motions} of the group.

    \

    A connected Lie group $\widetilde{G}$ that has a homomorphic map 
    $\Phi: \widetilde{G} \rightarrow G$ to a connected Lie group $G$ is called a
    \emph{covering group} if their associated Lie algebras are isomorphic by the map 
    $\varphi: \widetilde{\frak{g}} \rightarrow \frak{g}$. 

    \simbolo{$\widetilde{G}$}{Covering group}
    
    \ 
 

    The \emph{center} of $\frak{g}$ is the set of elements $X \in \frak{g}$ that commute
    with all the elements $Y \in \frak{g}$, defined as

    \begin{align}
        Z(\frak{g}) := \{ X \in \frak{g} : [X, Y] = 0 \quad \forall Y \in \frak{g} \}
                                                                          \label{grp:center}
                                                                          \..
    \end{align}

    \simbolo{$Z(\frak{g})$}{Center of $\frak{g}$}
    \simbolo{$Z_\frak{s}(\frak{g})$}{Centralizer of $\frak{g}$ with respect to $\frak{s}$}
    \

    The \emph{centralizer} of $\frak{g}$ with respect to $\frak{s}$ are those that
    commute with all $Y \in \frak{s}$

    \begin{align}
        Z_\frak{s}(\frak{g}) 
        := \{ X \in \frak{g} : [X, Y] = 0 \quad \forall Y \in \frak{s} \}
                                                                     \label{grp:centralizer}
                                                                     \..
    \end{align}

    The special homomorphism of a Lie algebra $\frak{g}$ onto a general vector space%
    \footnote{Technically this homomorphism maps $\frak{g}$ onto the general linear 
    algebra $\frak{gl}(V)$.}
    $V$ done by the linear map $\varphi(X) : V \rightarrow V$, $X \in \frak{g}$ (remember 
    that a Lie algebra is, in fact, a vector space), so that $\varphi(aX + Y) = 
    a \varphi(X) + \varphi(Y)$, is named a \emph{representation of} $\frak{g}$. A
    representation $\varphi$ is \emph{faithful} if the kernel of it is precisely 0, i.e.,
    $\ker \varphi = 0$.

    \

    The most usual representation of a Lie algebra is the \emph{adjoint} representation,
    denoted by $\ad_X$, for $X \in \frak{g}$, which takes $\frak{g}$ back into
    itself, i.e. $\ad_X: \frak{g} \rightarrow \frak{g}$, such that 
    
    \begin{align*}
        \ad_X Y := [X,Y]
        \..
    \end{align*}

    \simbolo{$\ad_X$}{Adjoint}

    This implies that the adjoint with relation to the commutator satisfies

    \begin{align}
        \ad_{[X,Y]} = \ad_X \circ \ad_Y - \ad_Y \circ \ad_X 
                                                           \label{grp:adjointRepresentation}
                                                           \,,
    \end{align}

    \noindent
    where ``$\circ$'' is the composition symbol. The kernel of $\ad_X$ is precisely the 
    center of $\frak{g}$.

    \simbolo{$\circ$}{Composition operation}

    \

    With that we can define the \emph{Killing form} of $\frak{g}$ as the symmetric
    bilinear form

    \begin{align}
        \langle X, Y \rangle := \Tr( \ad_X \circ \ad_Y )
                                                                     \label{grp:killingForm}
    \end{align}

    \simbolo{$\langle \cdot, \cdot \rangle$}{Killing form}

    \noindent
    for $X, Y \in \frak{g}$, which is invariant under all automorphisms, that is, if
    $\varphi$ is an automorphism

    \begin{align*}
        \langle \varphi(X), \varphi(Y) \rangle = \langle X,Y \rangle
        \..
    \end{align*}

    \

    Sometimes the Killing form is denoted by $\kappa(X,Y)$.

    \

    If $\frak{g}_1$ and $\frak{g}_2$ are two distinct Lie algebras, we denote a composition
    of both vector spaces by the pair $(X, Y)$, for $X \in \frak{g}_1$ and $Y \in
    \frak{g}_2$. The \emph{direct sum} $\frak{g}_1 \oplus \frak{g}_2$ is then defined such
    that the bracket operation is done componentwise

    \simbolo{$\oplus$}{Direct sum}

    \begin{align}
        [(X_1, Y_1), (X_2, Y_2)] = ( [X_1, X_2], [Y_1, Y_2] )
                                                                       \label{grp:directSum}
                                                                       \,,
    \end{align}

    \noindent
    where we identify the components of each subalgebra $\frak{g}_1: (X, 0)$ and
    $\frak{g}_2: (0, Y)$. Moreover, both subalgebras are ideals of the direct sum
    ($\frak{g}_1 \triangleleft (\frak{g}_1 \oplus \frak{g}_2)$ and
    $\frak{g}_2 \triangleleft (\frak{g}_1 \oplus \frak{g}_2)$), so they have null
    ``intersection'' $[\frak{g}_1, \frak{g}_2] = 0$.

    \

    If a Lie algebra $\frak{g}$ has an Abelian subalgebra, then we can always decompose it 
    as

    \begin{align*}
        \frak{g} = \frak{g}_0 + Z(\frak{g})
    \end{align*}

    \noindent
    where $\frak{g}_0$ is the centerless subalgebra comprised of all the remaining,
    non-commuting elements of $\frak{g}$.

    \

    An algebra is called \emph{semi-simple} if it has no Abelian ideals. Similarly, an
    algebra is called \emph{simple} if it has no ideals at all, except for the trivial $0$
    and $\frak{g}$.

    \

    The \emph{derived} subalgebra $\frak{g}'$ of the algebra $\frak{g}$ is the ideal of the
    comutator, i.e. $\frak{g}' \triangleleft [\frak{g}, \frak{g}]$, spanning through the
    subspace of all $[X,Y]$, $\forall X,Y \in \frak{g}$. Specifically 

    \simbolo{$\frak{g}',\frak{g}^{(r)}$}{Derived series}
    
    \begin{align*}
        \frak{g}' := [\frak{g}, \frak{g}]
        \..
    \end{align*}

    The \emph{derived series} is formed by successive derived subalgebras; commutators
    within commutators $\frak{g}'' := [[X,Y],[A,B]]$, $\forall X,Y,A,B \in \frak{g}$. Thus,
    for the $r$-th order

    \begin{align}
        \frak{g}^{(r)} := [\frak{g}^{(r-1)}, \frak{g}^{(r-1)}]
                                                                   \label{grp:derivedSeries}
                                                                   \..
    \end{align}

    \emph{g} is \emph{solvable} if the derived series goes to 0 up to the $r$-th
    order, that is, in that order, we reach an Abelian subgroup. All orders of the derived
    series are ideals of $\frak{g}$

    \begin{align*}
        \frak{g}^{(r)} \triangleleft \frak{g}^{(r-1)}
                       \triangleleft \ldots 
                       \triangleleft \frak{g}' \triangleleft \frak{g}
        \..
    \end{align*}

    The \emph{lower central series} is defined inductively by $\frak{g}^1 := \frak{g}'$

    \simbolo{$\frak{g}^{r}$}{Lower central series}

    \begin{align}
        \frak{g}^{r+1} &:= [ \frak{g}, \frak{g}^r ]
                                                                                \nonumber \\
        &:= [ \frak{g}, [ \frak{g}, \frak{g}^{r-1} ]]
                                                                                \nonumber \\
        &:= [ \frak{g}, [ \frak{g}, [ \frak{g}, [ \ldots [ \frak{g}, \frak{g} 
                                                         ] \ldots ] ] ]
                                                              \label{grp:lowerCentralSeries}
                                                              \,,
    \end{align}

    \noindent
    or

    \begin{align*}
        \frak{g}^{r} = \ad^{r}_\frak{g} \frak{g} 
                     = \underbrace{(\ad_\frak{g} \ldots \ad_\frak{g})
                                  }_{r\ \text{times}}\frak{g}
        \,,
    \end{align*}

    \noindent
    and are also all ideals of $\frak{g}$. We say \emph{g} is \emph{Nilpotent} if $g^r =
    0$. If $\frak{g}$ is Nilpotent, then it is also solvable.

    \

    The \emph{radical} of $\frak{g}$, $R(\frak{g})$, is the ideal of $\frak{g}$ that
    contains \emph{all} solvable ideals, that is, it is the largest solvable ideal of 
    $\frak{g}$.

    \simbolo{$R(\frak{g})$}{Radical of $\frak{g}$}

    \

    Finally, we define the \emph{complexification} of a real Lie algebra $\frak{g}$,
    denoted by $\frak{g}_\mathds{C}$ by composing a complex algebra as

    \simbolo{$\frak{g}_\mathds{C}$}{Algebra complexification}

    \begin{align}
        Z &= X_1 + i X_2
        \ \,, \quad X_1, X_2 \in \frak{g}
                                                                \label{grp:complexification}
    \end{align}

    \noindent
    such that the bracket operation on $\frak{g}$ has an unique extension to
    $\frak{g}_\mathds{C}$ as

    \begin{align*}
        [Z_1, Z_2] &= [ X_1 + i X_2, Y_1 + i Y_2] 
        \\
                   &= ([X_1,Y_1] - [X_2, Y_2])
                      + i ( [X_1, Y_2] + [X_2, Y_1] )
    \end{align*}

    \noindent
    for $Z_1, Z_2 \in \frak{g}_\mathds{C}$ and $X_1, X_2, Y_1, Y_2 \in \frak{g}$. The
    Jacobi identity evidently is preserved in this extension.

    \

    \simbolo{$\frak{g}_\mathds{R}$}{Algebra realification}

    On the other hand, the \emph{realification} or \emph{de-complexification} 
    $\frak{g}_\mathds{R}$ of a complex Lie algebra $\frak{g}$ can be done by observing that 
    an element $Z = A + iB$, $Z \in \frak{g}$ where $A, B$ are real matrices has a matrix 
    representation

    \begin{align*}
        Z = \left(
            \begin{array}{c|c}
                A & -B \\ \hline
                B & A
            \end{array}
        \right)
    \end{align*}

    \noindent
    of double dimension, identifying who is $A$ and $B$ in that canonical form. This can be
    thought as a decomposition into the basis of Pauli matrices 
    $ Z = \sigma_0 \otimes A -i \sigma_2 \otimes B$, where $\otimes$ is the
    \emph{Kroenecker product of matrices}, and 
    $\sigma_0 \equiv \mathds{1} = \left(\begin{matrix} 1 & 0 \\ 0 & 1 \end{matrix}\right)$
    and $\sigma_2 = \left(\begin{matrix} 0 & -i \\ i & 0 \end{matrix}\right)$.
}}}

    \fancyend
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ ------------ // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
