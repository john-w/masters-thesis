%%%%%%%%%%%%%%%%%%%%%%%%%%% -- APPENDIX - CALCULUS OF VARIATIONS -- %%%%%%%%%%%%%%%%%%%%%%%%
{
    The \emph{Principle of Least Action} is one of the hallmarks of physics which enabled
    us to formulate and solve numerous mechanical systems of discrete particles in
    lagrangian and hamiltonian mechanics and, later, to extend the approach to infinite
    degrees of freedom, introducing the discipline of \emph{Classical Theory of Fields},
    thus giving a new brath to the study of the fundamental interations of Nature.    
    \

    \simbolo{$I$}{Action}
    \simbolo{$L$}{Lagrangian}
    
    We define the \emph{Action} functional $I$ as a parametric integral of some function of 
    parameter $\tau$, the $N-1$ generalized coordinates $q$ and its velocities 
    $\dot{q}$, denoted by $L = L(q, \dot{q}; \tau)$ between the interval $(\tau_1, \tau_2)$ 
    in a $(N-1)$-dimensional space,

    \begin{align}
        I[q] := \int_{\tau_1}^{\tau_2} d\tau\ L(q, \dot{q}; \tau)
                                                                       \label{var:actionDef}
                                                                       \,,
    \end{align}

    \noindent
    which define a \emph{trajectory} between said interval in the configuration space
    defined by the generalized coordinates. $L$ is called the \emph{Lagrangian} of the
    system.  It is also defined the operation of \emph{variation} $\delta$, which represents
    a small change of the object in question.

    \begin{figure}[h!]
        \centering
        \includegraphics[width=.15\textwidth]{res/Var__VariationalPrinciple.pdf}
        \captionof{figure}{Of all possible trajectories, only one represents the actual
        physical trajectory: the one that exttrimizes the action $I$.}
        \label{fig:var:principleOfLeastAction}
    \end{figure}
    
    \

    The principle of least action is
    stated as: \emph{The physical trajectory of any physical system is such that the action
    is stationary, i.e., $\delta I = 0$}.

    \

    This condition is sufficient to find the \emph{equations of motion} for pretty much any
    physical system in its most general form, taking into account constraint forces and
    whatnot. As we will only be interested in a particular subset of problems that does
    not have such peculiarities\footnote{We will only briefly study one such case in a
    moment.}, we can safely impose the usual extra condition of
    \emph{fixed endpoints}, that is,

    \begin{align}
        \delta q(\tau_1) = 0 = \delta q(\tau_2)
                                                                  \label{var:fixedEndPoints}
                                                                  \,.
    \end{align}

    When this principle is applied to \eqref{var:actionDef}, using condition 
    \eqref{var:fixedEndPoints}, we obtain the the famous set of $N-1$ \emph{Euler-Lagrange
    equations}

    \begin{align}
        \sum\limits_i
        \dpar{L}{q^i} - \frac{d}{d\tau} \bigg( \dpar{L}{\dot{q}^i} \bigg) = 0
                                                          \label{var:EulerLagrangeEquations}
                                                          \,.
    \end{align}

    If we consider the Lagrangian in the continuum, the number of ``discrete particles''
    (degrees of freedom) will go to infinity and the usual sum over the $i$ particles will
    transform into a spatial integral over the whole space, effectively turning such
    functional into a \emph{density} to be integrated. This limit allows us to rewrite
    \eqref{var:actionDef} as

    \begin{align}
        I[T]
        := \int_{\tau_1}^{\tau_2} d\tau \int_V d^{N-1}x\ \mathcal{L}(T,\tpartial_i T;\tau)
                                                                  \label{var:fieldActionDef}
    \end{align}

    \simbolo{$\mathcal{L}$}{Lagrangian density}

    \noindent
    where $\mathcal{L}$ is the \emph{Lagrangian Density}, function of some tensor
    \emph{field} $T$ and the parameter $\tau$, the proper time. However, this definition is 
    not invariant by a change of coordinates. Since there are $(N-1)$ diferentials $dx^i$,
    to make it so, we have to take into account the Jacobian 
    \eqref{geodiff:metricTensorDeterminant} that will take care of any coordinate 
    transformation. With all that, \eqref{var:fieldActionDef} becomes

    \begin{align}
        I[T] = \int_V d^Nx\sqrt{g}\ \mathcal{L}(T, \tpartial_\mu T,x^\mu)
                                                                 \label{var:fieldActionNDef}
                                                                 \,,
    \end{align}

    \noindent
    with $\mu = 0, 1, \ldots, N-1$, $x^0 \equiv \tau$, and now explicitly turns out to be 
    a function of the $N$ coordinates. Furthermore, the condition of fixed endpoints 
    \eqref{var:fixedEndPoints} gets replaced by a null variation on the border 
    $\tpartial V$, that is

    \begin{align}
        \delta x^\mu \bigg|_{\tpartial V} = 0
                                                                     \label{var:fixedBorder}
                                                                     \..
    \end{align}

    \simbolo{$\partial V$}{Border of $V$}
    
    So, by taking that into account and applying the principle of least action, one gets the 
    Euler-Lagrange equations for tensor fields

    \begin{align}
        \dpar{\mathcal{L}}{\tT[^{\alpha\ldots}_{\beta\ldots}]}
        - \frac{1}{\sqrt{g}} \tpartial_\mu \bigg(
            \sqrt{g}
            \dpar{\mathcal{L}}{(\tpartial_\mu \tT[^{\alpha\ldots}_{\beta\ldots}])}
        \bigg)
        = 0
                                                     \label{var:EulerLagrangeFieldEquations}
                                                     \..
    \end{align}

    Now let us return to the main chain of thoughts.

    \

    Since we are interested in physical trajectories, \eqref{geodiff:lineElement} is the 
    natural choice for the lagrangian $L$, because it already describes the path some
    particle partakes and it is invariant. Hence by choosing

    \begin{align}
        L d\tau = ds &\equiv \sqrt{g_{ij} dx^i dx^j}
                                                                                \nonumber \\
        &=
        \sqrt{g_{ij} \frac{dx^i}{d\tau} \frac{dx^j}{d\tau}} d\tau
                                                                                \nonumber \\
                                                                                \nonumber \\
        \implies
        L
        &=
        \sqrt{g_{ij} \frac{dx^i}{d\tau} \frac{dx^j}{d\tau}}
                                                          \label{var:freeParticleLagrangian}
                                                          \,,
    \end{align}

    \noindent
    and plugging it back into \eqref{var:EulerLagrangeEquations}, remembering that $g_{ij}
    \dot{x}^i \dot{x}^j = c^2 \equiv 1$,

    \begin{align*}
        \dpar{L}{\dot{x}^a} 
        &= 
\iftoggle{steps}{
        \bigg(g_{lm} \dot{x}^l \dot{x}^m \bigg)^{-\frac{1}{2}}
        g_{aj} \dot{x}^j
                                                                                \nonumber \\
        &=
}
        g_{aj} \dot{x}^j
                                                                                \nonumber \\
        \rightarrow
        \frac{d}{d\tau} \bigg(
        \dpar{L}{\dot{x}^a} \bigg)
        &= 
        g_{aj} \ddot{x}^j +
        \frac{1}{2} \bigg(g_{aj,k} + g_{ak,j}\bigg)\dot{x}^j \dot{x}^k
    \end{align*}

    \noindent
    and

    \begin{align*}
        \dpar{L}{x^a} 
        &=
\iftoggle{steps}{
        \frac{1}{2} 
        \bigg(g_{lm} \dot{x}^l \dot{x}^m \bigg)^{-\frac{1}{2}}
        g_{ij,a} \dot{x}^i \dot{x}^j
                                                                                \nonumber \\
    &=
}
        \frac{1}{2} g_{ij,a} \dot{x}^i \dot{x}^j
        \..
    \end{align*}

    Therefore,

    \begin{align*}
        g_{aj} \ddot{x}^j + \frac{1}{2} &\bigg(
                g_{aj,k} + g_{ak,j} - g_{ik,a}
        \bigg) \dot{x}^j  \dot{x}^k
        = 0
    \end{align*}

    \noindent
    which multiplied by $g^{ia}$ gives

    \begin{align*}
        \ddot{x}^i + \Gamma^i_{jk} \dot{x}^j \dot{x}^k 
        = 0
        \..
    \end{align*}

    \noindent
    This is precisely the geodesic equation \eqref{geodiff:geodesicEquation} derived in
    the absence of constraint forces, confirming that the shortest trajectory possible one
    particle describes in free-fall on a Riemannian manifold is indeed the geodesic curve.

    \

    Now, to properly formulate the Euler-Lagrange equations for gravitational fields, we 
    first need to choose a good candidate for the Lagrangian and assure that it
    satisfies the desirable conditions of invariance, which has to depend on the fundamental
    quantity in question, the metric tensor itself, of course. So by taking
    variations of it and imposing the variational principle,
    
    \begin{align}
        \frac{\delta I}{\delta g} = 0
                                                   \label{var:variationalPrincipleForMetric}
                                                   \,,
    \end{align}

    \noindent
    we shall get the field equations we are looking for. One such invariant candidate
    is precisely the Ricci scalar $R$ \eqref{geodiff:RicciScalar}, which by construction
    depends solely on the metric and its derivatives, and is of class $\mathcal{C}^2$, it is 
    inherently invariant by virtue of it being a scalar and it is actually a \emph{density} 
    just like the Lagrangian density. We then propose the following

    \begin{align}
        \mathcal{L} = R + \alpha \mathcal{L}_M
                                                       \label{var:proposedLagrangianDensity}
                                                       \,,
    \end{align}

    \noindent
    where $\alpha$ is a constant and $\mathcal{L}_M$ the Lagrangian density of matter 
    to be determined later on.

    \

    Expression \eqref{var:fieldActionNDef} gets broken down into two separate actions to be
    minimized,

    \begin{align}
        I = I_G + I_M
                                                                 \label{var:proposedActions}
                                                                 \,;
    \end{align}

    \noindent
    the \emph{geometrical} term

    \begin{align}
        I_G[g] = \int_V d^N x \sqrt{g}\ R
                                                                         \label{var:I_G_def}
                                                                         \,,
    \end{align}

    \noindent
    called \emph{Einstein-Hilbert action}, and the generic \emph{matter} term to be 
    determined

    \begin{align}
        I_M[g] = \alpha \int_V d^N x \sqrt{g}\ \mathcal{L}_M
                                                                         \label{var:I_M_def}
                                                                         \,,
    \end{align}

    \noindent
    are both functionals of $g_{\mu\nu}$. Let us first address the matter term. Applying a 
    variation to it, we get

    \begin{align}
        \delta I_M 
        &= 
        \alpha
        \delta \int_V d^N x \sqrt{g}\ \mathcal{L}_M
                                                                                \nonumber \\
        &=
        \alpha
        \int_V d^N x \bigg( 
            \dpar{ \sqrt{g}\ \mathcal{L}_M }{ g_{\mu\nu} } \delta g_{\mu\nu}
            + \dpar{ \sqrt{g}\ \mathcal{L}_M }{ g_{\mu\nu,\sigma} } \delta g_{\mu\nu,\sigma}
        \bigg)
                                                                                \nonumber \\
        &=
        \alpha
        \int_V d^N x \bigg[ 
            \dpar{ \sqrt{g}\ \mathcal{L}_M }{ g_{\mu\nu} } 
            - \dpar{}{x^\sigma} 
                \bigg(
                    \dpar{ \sqrt{g}\ \mathcal{L}_M }{ g_{\mu\nu,\sigma} }
                \bigg)
        \bigg] \delta g_{\mu\nu}
                                                                                \nonumber \\
                                                                              &\qquad \qquad
        + \dpar{ \sqrt{g}\ \mathcal{L}_M }{ g_{\mu\nu} }
        \underbrace{ \delta g_{\mu\nu}\bigg|_{\partial V} }_{= 0}
                                                                                \nonumber \\
        &=
        \alpha
        \int_V d^N x \sqrt{g}\ T^{\mu\nu} \delta g_{\mu\nu}
                                                                    \label{var:matterAction}
                                                                    \,,
    \end{align}

    \noindent
    where we define the clearly symmetric energy-momentum tensor by

    \begin{align}
        \sqrt{g}\ T^{\mu\nu}
        &:=
        \dpar{ \sqrt{g}\ \mathcal{L}_M }{ g_{\mu\nu} } 
            - \dpar{}{x^\sigma} 
                \bigg(
                    \dpar{ \sqrt{g}\ \mathcal{L}_M }{ g_{\mu\nu,\sigma} }
                \bigg)
                                                            \label{var:energyMomentumTensor}
                                                            \..
    \end{align}

    Now, for $I_G$,

    \begin{align*}
        \delta I_G
        &=
        \delta \int_V d^N x \sqrt{g}\ R
                                                                                \nonumber \\
        &=
        \int_V d^N x\ \delta(\sqrt{g}\ R)
                                                                                         \,,
    \end{align*}

    \noindent
    we need to elaborate a wee bit more. The variation in question has the following form

    \begin{align*}
        \delta(\sqrt{g}\ R)
        = \delta(\sqrt{g}\ g^{\mu\nu} R_{\mu\nu})
        = \delta(\sqrt{g}\ g^{\mu\nu}) R_{\mu\nu} 
            + \sqrt{g}\ g^{\mu\nu} \delta R_{\mu\nu}
                                                                                         \,,
    \end{align*}

    \noindent
    so more terms will pop out from it and shall be pre-computed next:

    \begin{align*}
        \delta g
        &=
        \delta (\det \mathds{G})
                                                                                \nonumber \\
        &=
        \delta e^{\log\det \mathds{G}}
\iftoggle{steps}{
                                                                                \nonumber \\
        &=
        \delta e^{\text{tr} \log \mathds{G}}
}{}
                                                                                \nonumber \\
        &=
        e^{\text{tr} \log \mathds{G}}
        \delta (\text{tr} \log \mathds{G})
\iftoggle{steps}{
                                                                                \nonumber \\
        &=
        \det \mathds{G}\ \text{tr}(\mathds{G}^{-1} \delta \mathds{G})
}{}
                                                                                \nonumber \\
        &=
        g g^{\mu\nu} \delta g_{\mu\nu}
        \,,
    \end{align*}

    \
\iftoggle{steps}{

    \fancybreak
    \vspace{-.3in}

    \

    \begin{align*}
        \delta \sqrt{g}
        &=
        \frac{1}{2\sqrt{g}} \delta g
                                                                                \nonumber \\
        &=
        \frac{1}{2\sqrt{g}} g g^{\mu\nu} \delta g_{\mu\nu}
                                                                                \nonumber \\
        &=
        \frac{1}{2} \sqrt{g}\ g^{\mu\nu} \delta g_{\mu\nu}
    \end{align*}

    \

    \fancybreak
    \vspace{-.3in}

}
    \

    \begin{align*}
        \delta \delta^\mu_\nu \equiv 0
        &=
        \delta (g^{\mu\lambda} g_{\lambda\nu})
                                                                                \nonumber \\
        &=
        (\delta g^{\mu\lambda}) g_{\lambda\nu} 
            + g^{\mu\lambda} (\delta g_{\lambda\nu})
                                                                                \nonumber \\
                                                                                \nonumber \\
        \rightarrow
        \delta g^{\mu\nu}
        &=
        - g^{\mu\alpha} g^{\nu\beta} \delta g_{\alpha\beta}
        \..
    \end{align*}

    Plugging everything back together yields

    \begin{align}
        \delta( \sqrt{g}\ g^{\mu\nu} ) R_{\mu\nu}
        &=
        \bigg[
            (\delta \sqrt{g}) g^{\mu\nu} + \sqrt{g} (\delta g^{\mu\nu})
        \bigg] R_{\mu\nu}
\iftoggle{steps}{
                                                                                \nonumber \\
        &=
        \bigg[
            \bigg(
                \frac{1}{2} \sqrt{g} g^{\alpha\beta} \delta g_{\alpha\beta}
            \bigg)\ g^{\mu\nu} 
            - \sqrt{g} g^{\mu\alpha} g^{\nu\beta} \delta g_{\alpha\beta}
        \bigg] R_{\mu\nu}
                                                                                \nonumber \\
        &=
        \sqrt{g}
        \bigg(
              \frac{1}{2} g^{\alpha\beta} g^{\mu\nu} R_{\mu\nu}
            - g^{\mu\alpha} g^{\nu\beta} R_{\mu\nu}
        \bigg) \delta g_{\alpha\beta}
}
                                                                                \nonumber \\
        &=
        - \sqrt{g}
        \bigg(
            R^{\alpha\beta}
            - \frac{1}{2} g^{\alpha\beta} R
        \bigg) \delta g_{\alpha\beta}
                                                               \label{var:metricsVariations}
                                                               \,.
    \end{align}

    It remains to be shown that $\delta R_{\mu\nu}$ does not contribute in anything, that
    is, it can be put on a total derivative so that it vanishes by virtue of
    \eqref{var:fixedBorder}. To demonstrate this, let us again consider a geodesic frame of
    reference, so that

    \begin{align*}
        g^{\mu\nu} \delta R_{\mu\nu}
        &=
        g^{\mu\nu} \delta( \Gamma^\lambda_{\mu\nu,\lambda} -
                                \Gamma^\lambda_{\mu\lambda,\nu})
                                                                                \nonumber \\
        &=
        g^{\mu\nu} \delta \Gamma^\lambda_{\mu\nu,\lambda}
        - g^{\mu\lambda} \delta \Gamma^\nu_{\mu\nu,\lambda}
                                                                                \nonumber \\
        &=
        \dpar{}{x^\lambda} \bigg(
            g^{\mu\nu} \delta \Gamma^\lambda_{\mu\nu}
            - g^{\mu\lambda} \delta \Gamma^\nu_{\mu\nu}
        \bigg)
                                                                                \nonumber \\
        &=
        \dpar{w^\lambda}{x^\lambda}
                                                                                         \,,
    \end{align*}

    \noindent
    where we temporarily defined the vector $w^\lambda$ as the expression inside the
    brackets to save us some ink. This represents de divergence of $w^\lambda$ in the
    geodesic frame, but since we are dealing with tensor equations, the result is valid
    in all frames given that we use the generalized divergence instead. So,

\iftoggle{steps}{
    \begin{align*}
        \tw[^\lambda_{,\lambda}] 
        &\rightarrow \tw[^\lambda_{;\lambda}] 
        =
        \tw[^\lambda_{,\lambda}] 
        + \Gamma^\lambda_{\kappa\lambda} w^\kappa
                                                                                \nonumber \\
                                                                                \nonumber \\
        \Gamma^\lambda_{\kappa\lambda}
        &=
        \frac{1}{2} g^{\lambda\sigma} \bigg(
            \cancel{g_{\kappa\sigma,\lambda}} + g_{\sigma\lambda,\kappa}
            - \cancel{g_{\kappa\lambda,\sigma}}
        \bigg)
                                                                                \nonumber \\
        &=
        \frac{1}{2} g^{\lambda\sigma} g_{\sigma\lambda,\kappa}
                                                                                \nonumber \\
        &=
        \frac{1}{2} g^{-1} \dpar{g}{x^\kappa}
                                                                                \nonumber \\
                                                                                \nonumber \\
                                                                                \nonumber \\
        \therefore
        \tw[^\lambda_{;\lambda}] 
        &=
        \frac{1}{\sqrt{g}} \sqrt{g} \tw[^\lambda_{,\lambda}] 
        +
        \frac{1}{2\sqrt{g}} \frac{1}{\sqrt{g}} \dpar{g}{x^\lambda} w^\lambda
                                                                                \nonumber \\
        &=
        \frac{1}{\sqrt{g}} \dpar{}{x^\lambda} \bigg(
            \sqrt{g} w^\lambda
        \bigg)
    \end{align*}
}{}

    \begin{align*}
        g^{\mu\nu} \delta R_{\mu\nu} = 
        \frac{1}{\sqrt{g}} \dpar{}{x^\lambda} \bigg( \sqrt{g} w^\lambda \bigg)
        \,,
    \end{align*}

    \noindent
    and, when going back to the action integral, we have

    \begin{align*}
        \int_V d^N x \sqrt{g}\ \bigg(
            \frac{1}{\sqrt{g}} \dpar{}{x^\lambda} \bigg( \sqrt{g} w^\lambda \bigg)
        \bigg)
        &=
        \bigg( \sqrt{g} w^\lambda \bigg)
                                                                    \bigg|_{\tpartial V}
        \equiv 0
        \,,
    \end{align*}

    \noindent
    since

    \begin{align*}
        w^\lambda \propto \delta \Gamma^\lambda_{\mu\nu}
        \propto \delta g_{\mu\nu,\kappa}
        \propto \dpar{}{x^\kappa} \delta g_{\mu\nu} = 0 
        \ {, on } \tpartial V
        \..
    \end{align*}

    Then, the variation of the geometric action becomes

    \begin{align}
        \delta I_G
        &=
        \int_V d^N x \sqrt{g} \bigg(
            \frac{1}{2} g^{\mu\nu} R - R^{\mu\nu}
        \bigg) \delta g_{\mu\nu}
                                                                 \label{var:geometricAction}
                                                                 \..
    \end{align}

    Finally, putting all together and applying the least action principle on the full
    action \eqref{var:proposedActions},

    \begin{align}
        \delta I = 0
        &= 
        \int_V d^N x \sqrt{g} \bigg(
            \frac{1}{2} g^{\mu\nu} R - R^{\mu\nu} + \alpha T^{\mu\nu}
        \bigg) \delta g_{\mu\nu}
                                                                                \nonumber \\
                                                                                \nonumber \\
        \implies
        & R^{\mu\nu} - \frac{1}{2} g^{\mu\nu} R = \alpha T^{\mu\nu}
                                                   \label{var:EinsteinHilbertFieldEquations}
                                                   \,,
    \end{align}

    \noindent
    which are the \emph{Einstein field equations} in the presence of matter. The only thing
    left behind was to determine $\alpha$ such that we recover the classic
    non-relativistic Newton's gravity law

    \begin{align}
        \nabla^2 \Phi = 4\pi G \rho_M
                                                   \label{var:classicalNewtonFieldEquations}
                                                   \,,
    \end{align}

    \simbolo{$\Phi$}{Static Newton gravitational potential}

    \noindent
    where $\rho_M$ is the density of matter and $\Phi$ is the \emph{static} gravitational
    potential. To do that we first have to note that 
    \eqref{var:EinsteinHilbertFieldEquations} can be put in the form 

    \begin{align}
        R^{\mu\nu} = \alpha \bigg( T^{\mu\nu} - \frac{1}{2} g^{\mu\nu} T \bigg)
                                       \label{var:EinsteinHilbertFieldEquations_functionOfT}
                                       \,,
    \end{align}

    \noindent
    by simply contracting said equation with $g_{\mu\nu}$\footnote{Remember that
    $g_{\mu\nu} g^{\mu\nu} = \text{tr} \mathds{1} = 4$ in the $(3+1)$-dimensional 
    spacetime.} in order to determine $R$ and feed it back to it.

    \

    The energy-momentum tensor carries the sources that generate the field. Now it 
    suffices to say that the only relevant component in the non-relativistic regime will be 
    $T^{00}$, which represents the rest-mass density

    \begin{align*}
        T_{00} &= \rho_M
                                                                                \nonumber \\
        T_{ij} &\approx 0
                                                                                \..
    \end{align*}

    In this regime, we still have the almost ``Minkowskian'' metric $g_{\mu\nu} 
    \rightarrow \text{diag}(g_{00}, -1, -1, -1)$, so that when returned into 
    \eqref{var:EinsteinHilbertFieldEquations_functionOfT}, it gives rise to the only 
    non-null Christoffel symbols, namely, 

    \begin{align*}
        \Gamma^k_{00} &= \frac{1}{2} \tg[_{00}^{,k}]
        \\
        \Gamma^0_{0k} &= \frac{1}{2} g^{00} g_{00,k}
        \..
    \end{align*}
    
    Here we are only interested in the the dominant terms contained in the Ricci tensor; 
    for the ${}_{00}$ component we have

    \begin{align*}
        R_{00} &\approx
        \Gamma^k_{00,k} - \cancel{\Gamma^k_{0k,0}}
                                                                                \nonumber \\
        &\approx
        \frac{1}{2} \tg[_{00}^{,k}_{,k}]
                                                                                \,,
    \end{align*}

    \noindent
    where all the other terms have a time derivative $\tpartial_0$ which vanishes for
    static fields. This laplacian of $g_{00}$ is still mysterious. To determine it, we
    return to the geodesic equation \eqref{geodiff:geodesicEquation} in the classical
    limit, where

    \begin{align*}
        u^\mu = (1, \boldsymbol{v})
        \,; \quad v^i \ll 1
        \..
    \end{align*}

    So, for spatial components

    \begin{align*}
        0
        &=
        \frac{d v^k}{d\tau} + \Gamma^k_{\alpha\beta} u^\alpha u^\beta
                                                                                \nonumber \\
        &=
        \frac{d v^k}{d\tau} + \Gamma^k_{00} u^0 u^0
                            +2\underbrace{\Gamma^k_{0k}}_{=0} u^0 u^k
                            + \Gamma^k_{ij} \underbrace{ u^i u^j
                                                       }_{\approx 0}
                                                                                \nonumber \\
        &=
        \frac{d v^k}{d\tau} + \Gamma^k_{00}
                                                                                \nonumber \\
        &=
        \frac{d v^k}{d\tau} - \frac{1}{2} \tg[_{00}^{,k}]
        \..
    \end{align*}

    Since $\boldsymbol{F} = - m \boldsymbol{\nabla} \Phi$, we have

    \begin{align*}
        \Phi = - \frac{1}{2} g_{00}
        \.,
    \end{align*}

    \noindent
    so that

    \begin{align*}
        R_{00} = -\tPhi[^{,k}_{,k}]
        \..
    \end{align*}

    Finally, going back to the Einstein equation,

    \begin{align*}
        R_{00}
        &=
        \alpha \bigg(T_{00} - \frac{1}{2} g_{00} T \bigg)
                                                                                \nonumber \\
        -\tPhi[^{,k}_{,k}]
        &=
        \alpha \bigg(T_{00} - \frac{1}{2} \underbrace{g_{00} g^{00}
                                                     }_{= 1}T_{00} \bigg)
                                                                                \nonumber \\
        &=
        \alpha \frac{1}{2} T_{00}
                                                                                \nonumber \\
                                                                                \nonumber \\
        \rightarrow
        - 4\pi G \rho_M
        &=
        \frac{\alpha}{2} \rho_M
                                                                                \nonumber \\
        \therefore
        \alpha
        &=
        - 8\pi G
        \..
    \end{align*}

    Now we are able to write the full Einstein equation:

    \begin{align}
        R^{\mu\nu} - \frac{1}{2} g^{\mu\nu} R = - 8\pi G T^{\mu\nu}
                                               \label{var:EinsteinHilbertFieldEquationsFull}
                                               \,,
    \end{align}

    \noindent
    or

    \begin{align}
        R^{\mu\nu} 
        = 
        - 8\pi G \bigg(
            T^{\mu\nu} - \frac{1}{2} g^{\mu\nu} T 
        \bigg)
                                   \label{var:EinsteinHilbertFieldEquationsFull_functionOfT}
                                   \..
    \end{align}


    
    \fancyend

}
%%%%%%%%%%%%%%%%%%%%%%%%%%% \\ --------------------------------- // %%%%%%%%%%%%%%%%%%%%%%%%
